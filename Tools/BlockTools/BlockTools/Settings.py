from os import path

class Settings:
    base_log_dir = r"c:\TetrisEvo";
    hc_log_dir = path.join(base_log_dir, "HillClimbing")
    hc_settings_dir = path.join(hc_log_dir, r"Settings")
    saved_state_path = path.join(hc_settings_dir, "savedState.txt")