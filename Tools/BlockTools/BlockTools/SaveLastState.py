﻿from Helpers import Helpers
from Settings import Settings
import re

rgx_generation = re.compile(".*Generation ([0-9]+)$")
rgx_individual = re.compile(".*[0-9]+: (.*)$")

def get_last_state(file_name):
    individuals = []
    generation = 0
    with open(file_name) as file:
            for line in file:
                # Match generations
                match = re.match(rgx_generation, line)
                if match != None:
                    individuals.clear()
                    generation = match.group(1)
                    continue
                # Match individuals in generation
                match = re.match(rgx_individual, line)
                if match != None:
                    individuals.append(match.group(1))
    return (generation, individuals)

if __name__ == "__main__":
    file_name = Helpers.find_last_evo(Settings.hc_log_dir)
    if file_name != None:
        (generation, individuals) = get_last_state(file_name)
        with open(Settings.saved_state_path, "w+") as saved_state:
            if individuals.count == 0:
                print("No individuals has been found in " + file_name + "\n")
                exit()
            else:
                saved_state.write(str(generation) + "\n")
                for ind in individuals:
                    saved_state.write(ind + "\n")
        print("Last state from "+ file_name +" saved successfully")
        #input("Press Enter to continue...")