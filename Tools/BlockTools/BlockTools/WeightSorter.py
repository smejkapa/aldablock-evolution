﻿import sys
sys.path.append("../BlockTools")

from os import path
from os import listdir
from os.path import isfile
from Settings import Settings
from Helpers import Helpers

#file_name = path.join(Settings.base_log_dir, "[2015-09-13-15-47-47]_bestIndividuals")
file_name = None
ext = ".txt"

def sort_file(file_name):
    with open(file_name + ext) as file:
        weights = []
        for line in file:
            weights.append(line.split(":"))
    weights.sort(key=lambda x:int(x[0]), reverse=True)
    with open(file_name + "_sorted" + ext, 'w+') as file:
        for w in weights:
            file.write(w[0] + ": " + w[1])

if __name__ == "__main__":
    if file_name is None:
        file_name = Helpers.find_last_evo(Settings.base_log_dir)[:-4] # remove .txt from the filename
    sort_file(file_name)
