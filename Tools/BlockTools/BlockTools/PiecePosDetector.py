import re;

file_name = "log.txt"
rgx_current_piece = re.compile("^update game this_piece_type ([A-Z])$")
rgx_position = re.compile("^update game this_piece_position (-?[0-9]),(-?[0-9])$")

pieces = {"I", "J", "L", "O", "S", "T"}
positions = {"I": (0,0), "J": (0,0), "L": (0,0), "O": (0,0), "S": (0,0), "T": (0,0)}

def process_file(file_name):
    with open(file_name) as file:
        current_piece = ""
        for line in file:
            match = re.match(rgx_current_piece, line);
            if match != None:
                current_piece = match.group(1)
                if current_piece in pieces:
                    pieces.remove(current_piece)
                continue
            match = re.match(rgx_position, line)
            if match != None and current_piece != "":
                x = match.group(1)
                y = match.group(2)
                positions[current_piece] = (x, y);
                current_piece = ""
    

if __name__ == "__main__":
    process_file(file_name)
    for key, value in positions.items():
        print(key + " " + str(value))