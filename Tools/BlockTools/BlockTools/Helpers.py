from os import path
from os import listdir
from os.path import isfile
from Settings import Settings

class Helpers:
    def find_last_evo(directory):
        best_indivs_files = [f for f in listdir(directory) 
                             if isfile(path.join(directory, f))
                             and "bestIndividuals" in f
                             and "sorted" not in f
                             and path.getsize(path.join(directory, f)) > 0] # non empty
        best_indivs_files.sort(reverse=True)
        if best_indivs_files.count != 0:
            return path.join(directory, best_indivs_files[0])
        else:
            print("There are no (non empty) bestIndividuals files in " + directory)
            return None