﻿//#define PLAY_ONLINE
using AldaBlockEvo.EvolutionNS;
using AldaBlockEvo.GameNS;
using AldaBlockEvo.OnlineGaming.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AldaBlockEvo
{
    class Program
    {
#if MULTIPLAYER
        static void PlayDuel()
        {
            const int GAME_COUNT = 1000;
            const int THREAD_COUNT = 8;

            RatingWeights w1 = new RatingWeights();
            RatingWeights w2 = new RatingWeights();
            //w1.ReadFromFile(GlobalSettings.WEIGHTS_TXT);
            w1.ReadFromString(GlobalSettings.CURRENT_WEIGHTS);
            w2.ReadFromString(GlobalSettings.NEW_WEIGHTS);

            string bot1Name = nameof(GlobalSettings.CURRENT_WEIGHTS);
            string bot2Name = nameof(GlobalSettings.NEW_WEIGHTS);

            Random rnd = new Random();

            int newWin = 0;
            int draw = 0;

            List<GameResult> results = new List<GameResult>();

            int gamesPlayed = 0;
            while(gamesPlayed != GAME_COUNT)
            {
                List<Task<GameResult>> games = new List<Task<GameResult>>();
                for (int i = 0; i < THREAD_COUNT; i++)
                {
                    List<IBot> bots = new List<IBot>();
                    bots.Add(new Bot(w1));
                    bots.Add(new Bot(w2));
                    Game game = new Game(bots.ToArray());
                    int seed = rnd.Next();
                    games.Add(Task<GameResult>.Factory.StartNew(() => { return game.StartGame(seed); }));
                }

                for (int i = 0; i < THREAD_COUNT; i++)
                {
                    ++gamesPlayed;
                    var result = games[i].Result;
                    results.Add(result);
                    if (result.Winner == Winner.Draw)
                    {
                        ++draw;
                    }
                    else if (result.Winner == Winner.First)
                    {
                        ++newWin;
                    }

                    Console.WriteLine($"%%%%%%%%%%%%% {gamesPlayed}/{GAME_COUNT} Game ended %%%%%%%%%%%%%");
                    Console.WriteLine($"Result: {(result.Winner == Winner.First ? bot1Name : result.Winner == Winner.Second ? bot2Name : "Draw")}");
                    Console.WriteLine($"Score: {result.Score[0]}/{result.Score[1]}");
                    Console.WriteLine($"Round: {result.RoundCount}");
                    Console.WriteLine($"Score is: ({bot1Name}/{bot2Name}) {newWin}/{gamesPlayed - newWin - draw} Ratio: {newWin / (double)(gamesPlayed - newWin - draw)}");
                    Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%");
                }
            }

            int newScore = results.Sum(x => x.Score[0]);
            int oldScore = results.Sum(x => x.Score[1]);

            Console.WriteLine();
            Console.WriteLine($"{bot1Name} won: {newWin} games");
            Console.WriteLine($"{bot2Name} won: {GAME_COUNT - newWin - draw} games");
            Console.WriteLine($"Draws: {draw}");
            Console.WriteLine($"Score ratio ({bot1Name}/{bot2Name}) {newScore}/{oldScore}");
            Console.WriteLine($"{bot1Name} had {newScore - oldScore} more score than {bot2Name}");
        }
#endif

        static void Main(string[] args)
        {
#if PLAY_ONLINE
            var botReceiver = new BotCommandReceiver();
            botReceiver.Run();
#else
            //CultureInfo.CurrentCulture = CultureInfo.InvariantCulture; // .NET 4.6
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture; // .NET 4.5.2
#endif

#if EVOLUTION
            Evolution evo = new Evolution();
            evo.Run();
#endif
#if HILL_CLIMBING
            bool continueFromFile = false;
            if (args.Length != 0)
            {
                if (args[0] == "-c")
                {
                    continueFromFile = true;
                }
            }
            HillClimbing hc = new HillClimbing();
            hc.Run(continueFromFile);
#endif
#if PLAY_SP
            
            RatingWeights weights = new RatingWeights(GlobalSettings.CURRENT_WEIGHTS);
            //weights.ReadFromFile($@"{GlobalSettings.LOG_DIR}weights.txt");
            Bot bot = new Bot(weights);
            
            Game game = new Game(new IBot[] { bot });
            
            Stopwatch gameSw = new Stopwatch();
            Random rnd = new Random();
            gameSw.Start();
            var result = game.StartGame(4);
            gameSw.Stop();

            Console.WriteLine($"Game finished ({result.RoundCount} rounds) in {gameSw.Elapsed.TotalMilliseconds}, avg round time: {gameSw.Elapsed.TotalMilliseconds / result.RoundCount}");
#endif
#if MULTIPLAYER
            PlayDuel();
#endif
        }
    }
}
