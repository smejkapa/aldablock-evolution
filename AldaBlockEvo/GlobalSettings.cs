﻿//#define PLAY_ONLINE

using System;
using AldaBlockEvo.GameNS;
using System.Collections.Generic;

namespace AldaBlockEvo
{
    static class GlobalSettings
    {

        // Game settings passed to the bot by the engine
        #region GameSettings
        public const int FIELD_WIDTH = 10;
        public const int FIELD_HEIGHT = 20;
        /// <summary>
        /// Time bank in ms
        /// </summary>
        public const int MAX_TIME_BANK = 10000;
        /// <summary>
        /// Time given to bot's time bank every round
        /// </summary>
        public const int TIME_PER_MOVE = 500;
        public const string BOT_NAME = "bot";
        #endregion

        /// <summary>
        /// Line addition computed via modulo, checking for 1, anything modulo 1 = 0
        /// </summary>
        private const int NEVER_ADD = 1;

        /// <summary>
        /// After how many rounds should we add a static line to the bottom of the field
        /// </summary>
        public const int STATIC_LINE_ADD_RATE = 20;
        public const int GARBAGE_LINE_ADD_RATE = 5;
        /// <summary>
        /// How much points do you need to send one line to your opponent?
        /// </summary>
        public const int SCORE_PER_GARBAGE_LINE = 4;

        #region Score
        public static readonly int[] N_LINE_CLEAR = { 1, 3, 6, 12 };
        public static readonly int[] T_SPIN = { 6, 12 };
        public const int PERFECT_CLEAR = 24;
        #endregion

        public const string BASE_LOG_DIR = @"C:\TetrisEvo\";
        public const string EVO_LOG_DIR = BASE_LOG_DIR + @"Evolution\";
        public const string HC_LOG_DIR = BASE_LOG_DIR + @"HillClimbing\";
        public const string HC_SETTINGS_DIR = BASE_LOG_DIR + @"HillClimbing\Settings\";
        public const string WEIGHTS_TXT = BASE_LOG_DIR + "weights.txt";
        public const int SCORE_RATING_IDX = 12;

        public static readonly int[] RATING_SIGN = new int[(int)CriteriaNames.CriteriaCount] { -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, 1 };

        private static readonly CriteriaNames[] IGNORE_SOME = new CriteriaNames[] { CriteriaNames.RemovedLines, CriteriaNames.MaximumWellDepth, CriteriaNames.Blocks };
        private static readonly CriteriaNames[] IGNORE_NONE = new CriteriaNames[] { };
        private static readonly CriteriaNames[] IGNORE_CRIT = IGNORE_NONE;
        public static readonly HashSet<CriteriaNames> IGNORE_CRIT_SET;

        public static readonly Move[] POSSIBLE_MOVES = (Move[])Enum.GetValues(typeof(Move));

        static GlobalSettings()
        {
            IGNORE_CRIT_SET = new HashSet<CriteriaNames>();
            for (int i = 0; i < IGNORE_CRIT.Length; i++)
            {
                IGNORE_CRIT_SET.Add(IGNORE_CRIT[i]);
            }

        }

#if !PLAY_ONLINE
        public static string DATE_TIME_FILE_FORMAT
            => $"{DateTime.Now:yyyy-MM-dd-HH-mm-ss}";
#endif

        // v29
        //public const string CURRENT_WEIGHTS = "(-39; 1.32),(-32; 0.55),(-93; 1.48),(12; 0.68),(-35; 0.94),(-57; 0.92),(-68; 1.32),(-42; 0.57),(-85; 0.55),(-86; 1.29),(-83; 1.41),(-92; 0.82),(30; 0.87)";
        // v31
        //public const string CURRENT_WEIGHTS = "(-1; 0.00325108599004561),(-7; 6.49886788032317),(0; 0.0171131719822111),(1; 0.0196372877298732),(1; 0.0433297326321448),(1; 0.0470965604847565),(1; 0.0473659863244864),(1; -1.68002666317176E-05),(1; 0.00132471439971237),(-32; 0.458903664704509),(-27; 0.540892800119544),(0; 0.00598526171296337),(1; 0.0398621897507868)";
        // v33
        //public const string CURRENT_WEIGHTS = "(-17; 1.22097363696726),(-1402; 0.649016125192214),(-10; 0.247297818515523),(14; 0.00364282491010486),(-10; 0.0229137695889595),(-17; 0.00114718396831721),(-10; 1.21171013643605),(-19; 0.0641429911650637),(-54; 0.0331106705437109),(-15; 0.528641357549353),(-17; 1.32756674397483),(-10; 0.00309114956341329),(0; 0.0798609661052894)";
        // v34
        //public const string CURRENT_WEIGHTS = "(-17; 1.22097363696726),(-1402; 0.649016125192214),(-10; 0.247297818515523),(14; 0.00364282491010486),(-10; 0.0229137695889595),(-17; 0.00114718396831721),(-10; 1.21171013643605),(-19; 0.0641429911650637),(-54; 0.0331106705437109),(-15; 0.528641357549353),(-17; 1.32756674397483),(-10; 0.00309114956341329),(1; 0.5)";
        // v38
        //public const string CURRENT_WEIGHTS = "(-17; 1.22097363696726),(-1402; 0.649016125192214),(-10; 0.247297818515523),(14; 0.00364282491010486),(-10; 0.0229137695889595),(-17; 0.00114718396831721),(-10; 1.21171013643605),(-19; 0.0641429911650637),(-54; 0.0331106705437109),(-15; 0.528641357549353),(-17; 1.32756674397483),(-10; 0.00309114956341329),(1; 2)";
        // v40 0.684 (vs previous version)
        public const string CURRENT_WEIGHTS = "(-314; 0.406697199948994),(-263; 1.19902674614881),(-108; 2.26019561915781),(15; 0.000159719665994723),(-7; 2.37875584681421),(-12; 0.0803167809096529),(-3; 3.79649983376141),(-2; -0.0164886447276053),(-333; -0.0132454273432808),(-68; 0.0311810145664656),(-39; 1.3921226735385),(-128; 0.108082387505704),(4; 2.89059116685407)";

        public const string NEW_WEIGHTS = "(-67; 0.186882067973628),(-213; 4.58786297506086),(-30; 0.463400507333524),(16; 0.12858425605287),(-58; 1.51050282717284),(-22; 1.875944745527),(-11; 2.33755967967468),(-10; 0.808501339749151),(1; 0.00171049843221998),(19; 0.479709560499839),(-127; 1.16811482432493),(-35; 0.270719068645025),(59; 1.16154250045899)";

        public const string NEW_WEIGHTS_OLDER = "(-3; 2.31354489990011),(-1120; 0.392947689222457),(-294; 2.57990253857157),(37; 0.0160829998655232),(-4; 0.200830999792565),(-5; 3.22895946809224),(-35; 2.01673958147535),(-7; 0.140825039642052),(-6; -0.0595758769768014),(-5; 0.396402942305206),(-159; 1.23216364816239),(-9; 1.23093705614382),(73; 1.60073706965333)";
        // 0.855
        //public const string NEW_WEIGHTS = "(0; -0.000442635741080612),(-7640; 0.775303553631185),(0; 0.928634846988725),(0; 0),(-128; 1.90705323282827),(0; 0),(-70; 2.15305520409091),(0; 0.137040080978246),(0; 0),(-90; 0.000455918276672408),(-328; 1.20806341632232),(-1; 0.0405494954675801),(8; 2.73388912712699)";
    }
}
