﻿using AldaBlockEvo.OnlineGaming;
using System;
using System.Collections.Generic;
using System.Text;

namespace AldaBlockEvo.GameNS
{
    enum FieldCellType : byte
    {
        Empty = 0,
        Piece,
        Block,
        Solid
    }

    struct PieceState : IEquatable<PieceState>
    {
        public Point Position { get; private set; }
        public int Rotation { get; private set; }
        public PieceState(Point position, int rotation)
        {
            Position = position;
            Rotation = rotation;
        }

        public bool Equals(PieceState other)
        {
            return Position.Equals(other.Position) && Rotation == other.Rotation;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            PieceState? ps = obj as PieceState?;
            if (ps == null)
            {
                return false;
            }
            return Equals(ps.Value);
        }
        public static bool operator ==(PieceState p1, PieceState p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(PieceState p1, PieceState p2)
        {
            return !p1.Equals(p2);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            unchecked
            {
                hash = hash * 23 + Position.GetHashCode();
                hash = hash * 23 + Rotation.GetHashCode();
            }
            return hash;
        }
    }

    enum AllMoves
    {
        Up,
        Down,
        Left,
        Right,
        TurnLeft,
        TurnRight,
        None
    }

    class Field
    {
        public FieldCell[,] Grid {get; private set; }
        public Piece Piece { get; set; }
        /// <summary>
        /// Marks the height from which the piece has been dropped last time.
        /// Useful to backtrack the drop move.
        /// </summary>
        private byte LastDrop { get; set; } // Not really needed since we don't use the drop ATM
        private static Random rnd;
        static Field()
        {
#if PLAY_SP
            rnd = new Random(1);
#else
            rnd = new Random();
#endif
        }
        // TODO create second (private) constructor (with init=false for clone) instead of init = true
        public Field(bool init = true)
        {
//#if PLAY_SP
//            rnd = new Random(1);
//#else
//            rnd = new Random();
//#endif
            Grid = new FieldCell[GlobalSettings.FIELD_WIDTH, GlobalSettings.FIELD_HEIGHT];
            LastDrop = 0;

            if (init)
            {
                InitGrid();
            }
        }

        private void InitGrid()
        {
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
                {
                    Grid[x, y] = new FieldCell(this, x, y, FieldCellType.Empty);
                }
            }
        }

        /// <summary>
        /// Adds a static line to the bottom of the field
        /// </summary>
        /// <returns>False if moving the field up causes the bot to lose the game, true otherwise</returns>
        public bool AddStaticLine()
        {
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                for (int y = 0; y < GlobalSettings.FIELD_HEIGHT - 1; y++)
                {
                    if (y == 0 && Grid[x,y].Type != FieldCellType.Empty)
                    {
                        return false;
                    }
                    Grid[x, y].Type = Grid[x, y + 1].Type;
                }
                Grid[x, GlobalSettings.FIELD_HEIGHT - 1].Type = FieldCellType.Solid;
            }
            return true;
        }
        public bool AddGarbageLine()
        {
            int empty = rnd.Next(GlobalSettings.FIELD_WIDTH);
            int solid = GlobalSettings.FIELD_HEIGHT;
            // Find the solid line
            for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                if (Grid[0, y].Type == FieldCellType.Solid)
                {
                    solid = y;
                    break;
                }
            }

            // Move all non solid lines by one cell up
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                for (int y = 0; y < solid - 1; y++)
                {
                    if (y == 0 && Grid[x, y].Type != FieldCellType.Empty)
                    {
                        return false;
                    }
                    Grid[x, y].Type = Grid[x, y + 1].Type;
                }
                if (x == empty)
                {
                    Grid[x, solid - 1].Type = FieldCellType.Empty;
                }
                else
                {
                    Grid[x, solid - 1].Type = FieldCellType.Block;
                }
            }
            return true;
        }

        public Field Clone()
        {
            Field clone = new Field(false)
            {
                LastDrop = LastDrop
            };
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
                {
                    clone.Grid[x, y] = Grid[x,y].Clone();
                    clone.Grid[x, y].Field = clone;
                }
            }
            clone.Piece = Piece.Clone();
            clone.Piece.ChangeField(clone);
            return clone;
        }
        internal void ProjectPiece()
        {
            foreach (var block in Piece.Blocks)
            {
                if (block.Position.Y >= 0)
                    Grid[block.Position.X, block.Position.Y].Type = FieldCellType.Piece;
            }
        }

        internal void BacktrackMove(AllMoves move)
        {
            switch (move)
            {
                case AllMoves.Up:
                    Piece.OneDown();
                    break;
                case AllMoves.Down:
                    Piece.NUp(1);
                    break;
                case AllMoves.Left:
                    Piece.Move(Direction.Right);
                    break;
                case AllMoves.Right:
                    Piece.Move(Direction.Left);
                    break;
                case AllMoves.TurnLeft:
                    Piece.TurnRight();
                    break;
                case AllMoves.TurnRight:
                    Piece.TurnLeft();
                    break;
                case AllMoves.None:
                    break;
                default:
                    break;
            }
        }

        internal bool IsPieceOut()
        {
            foreach (var block in Piece.Blocks)
            {
                if (block.Position.Y < 0)
                    return true;
            }
            return false;
        }

        public bool MakeMove(AllMoves move)
        {
            switch (move)
            {
                case AllMoves.Up:
                    Piece.NUp(1);
                    break;
                case AllMoves.Down:
                    Piece.OneDown();
                    break;
                case AllMoves.Left:
                    Piece.Move(Direction.Left);
                    break;
                case AllMoves.Right:
                    Piece.Move(Direction.Right);
                    break;
                case AllMoves.TurnLeft:
                    Piece.TurnLeft();
                    break;
                case AllMoves.TurnRight:
                    Piece.TurnRight();
                    break;
                case AllMoves.None:
                    break;
                default:
                    break;
            }
            return !Piece.IsOutOfBounds() && !Piece.IsColliding();
        }

        public bool MakeMove(Move move)
        {
            switch (move)
            {
                case Move.Drop:
                    LastDrop = Piece.Drop();
                    return true;
                case Move.Left:
                    Piece.Move(Direction.Left);
                    return !Piece.IsOutOfBounds() && !Piece.IsColliding();
                case Move.Right:
                    Piece.Move(Direction.Right);
                    return !Piece.IsOutOfBounds() && !Piece.IsColliding();
                case Move.Down:
                    Piece.OneDown();
                    return !Piece.IsOutOfBounds() && !Piece.IsColliding();
                case Move.TurnLeft:
                    Piece.TurnLeft();
                    return !Piece.IsOutOfBounds() && !Piece.IsColliding();
                case Move.TurnRight:
                    Piece.TurnRight();
                    return !Piece.IsOutOfBounds() && !Piece.IsColliding();
                default:
                    return false;
            }
        }
        public void BacktrackMove(Move move)
        {
            switch (move)
            {
                case Move.Drop:
                    Piece.NUp(LastDrop);
                    break;
                case Move.Left:
                    Piece.Move(Direction.Right);
                    break;
                case Move.Right:
                    Piece.Move(Direction.Left);
                    break;
                case Move.Down:
                    Piece.NUp(1);
                    break;
                case Move.TurnLeft:
                    Piece.TurnRight();
                    break;
                case Move.TurnRight:
                    Piece.TurnLeft();
                    break;
                default:
                    break;
            }
            // OPTIMIZE
            // We have returned into inconsistent state - that should not be possible
            if (Piece.IsOutOfBounds() || Piece.IsColliding())
            {
                throw new InvalidPieceStateException("Backtracking move into invalid state!");
            }
        }

        internal void PieceToBlock()
        {
            foreach (var block in Piece.Blocks)
            {
                Grid[block.Position.X, block.Position.Y].Type = FieldCellType.Block;
            }
        }
        
        internal FieldCellType GetCellType(Point point)
        { return Grid[point.X, point.Y].Type; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("------------------------------\n");
            for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                {
                    bool b = false;
                    foreach (var block in Piece.Blocks)
                    {
                        if(block.Position.X == x && block.Position.Y == y)
                        {
                            sb.Append(block.ToString());
                            b = true;
                            break;
                        }
                    }
                    if (!b)
                        sb.Append(Grid[x, y]);
                }
                sb.Append("\n");
            }
            sb.Append("------------------------------\n");
            return sb.ToString();
        }

        /// <summary>
        /// Determines whether there is a solid line or block underneath the piece
        /// </summary>
        /// <returns>False if the piece is loose in the field, true otherwise</returns>
        internal bool IsPieceGrounded()
        {
            foreach (var block in Piece.Blocks)
            {
                if (block.Position.Y == GlobalSettings.FIELD_HEIGHT - 1
                    || Grid[block.Position.X, block.Position.Y + 1].Type == FieldCellType.Solid
                    || Grid[block.Position.X, block.Position.Y + 1].Type == FieldCellType.Block)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Finds the highest field occupied (static or block) in field and returns its Y coordinate. Note: The lower the Y the higher the cell.
        /// </summary>
        /// <returns>Y coordinate of highest occupied cell in field. (= Lowest Y value)</returns>
        public int GetHighestYOccupied()
        {
            int highestY = GlobalSettings.FIELD_HEIGHT; // Remember that Y is 0 at the top and increases
            for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                {
                    if (Grid[x, y].Type == FieldCellType.Block
                        || Grid[x, y].Type == FieldCellType.Solid)
                    {
                        highestY = y;
                        break;
                    }
                }
                if (highestY != GlobalSettings.FIELD_HEIGHT)
                    break;
            }
            return highestY;
        }

        public int PrepareForSearch()
        {
            int highestY = GetHighestYOccupied();

            const int HEIGHT_BUFFER = 1; // 1 is minimal buffer
            int pieceBottom = Piece.GetLowestYPossible();
            int toMoveDown = highestY - pieceBottom - HEIGHT_BUFFER;

            for (int i = 0; i < toMoveDown; i++)
            {
                MakeMove(Move.Down);
            }

            return toMoveDown;
        }

        /// <summary>
        /// Computes all states where the piece is grounded. Does not exclude states to which the piece cannot get.
        /// </summary>
        /// <returns>All states where the piece is grounded.</returns>
        public IEnumerable<PieceState> GenerateAllGroundedStates()
        {
            Piece pieceBackup = Piece.Clone();

            HashSet<PieceState> allStates = new HashSet<PieceState>();
            HashSet<long> visited = new HashSet<long>();

            int startingY = Math.Max(GetHighestYOccupied() - 1, 0);

            bool[,] reachableFromTop = GetReachableFromTop();

            for (int y = startingY; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                // Early escape
                if (Grid[0, y].Type == FieldCellType.Solid)
                    break;

                for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                {
                    if (Grid[x, y].Type == FieldCellType.Block)
                        continue;

                    // There is a solid ground underneath
                    if (y == GlobalSettings.FIELD_HEIGHT - 1 || Grid[x, y + 1].Type == FieldCellType.Block || Grid[x, y + 1].Type == FieldCellType.Solid)
                    {
                        Point position = new Point(x, y);

                        // All rotations
                        for (int i = 0; i < Piece.RotationCount; i++)
                        {
                            // All possible positions
                            for (int blockIndex = 0; blockIndex < Piece.Blocks.Length; blockIndex++)
                            {
                                Piece.SetBlockPosition(blockIndex, position);
                                // Is this a valid grounded position?
                                if (!Piece.IsOutOfBounds() && !IsPieceOut() && !Piece.IsColliding())
                                {
                                    
                                    bool allReachable = true;
                                    foreach (var block in Piece.Blocks)
                                    {
                                        if (!reachableFromTop[block.Position.X, block.Position.Y])
                                        {
                                            allReachable = false;
                                            break;
                                        }
                                    }
                                    long pieceCode = Piece.GetBlocksCode();

                                    PieceState currentState = new PieceState(Piece.Position, Piece.Rotation);

                                    // Add new PieceState - this excludes duplicate states
                                    if (!visited.Contains(pieceCode))
                                    {
                                        allStates.Add(new PieceState(Piece.Position, Piece.Rotation));

                                        // Remove duplicate rotations if reachable from the top
                                        if (allReachable)
                                            visited.Add(pieceCode);

                                        /*Console.WriteLine("Noveeee!");
                                        Console.ReadLine();
                                        Console.WriteLine($"X: {currentState.Position.X} Y: {currentState.Position.Y} Rotation: {currentState.Rotation}");
                                        Console.WriteLine(this);*/
                                    }
                                }

                            }
                            Piece.TurnRight();
                        }
                    }
                }
            }

            Piece = pieceBackup;

            return allStates;
        }

        public bool[,] GetReachableFromTop()
        {
            bool[,] safe = new bool[GlobalSettings.FIELD_WIDTH, GlobalSettings.FIELD_HEIGHT];
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
                {
                    if (Grid[x, y].Type == FieldCellType.Empty)
                    {
                        safe[x, y] = true;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return safe;
        }

        public PieceState GetPieceState()
        {
            return new PieceState(Piece.Position, Piece.Rotation);
        }

        public void SetPieceState(PieceState state)
        {
            Piece.SetPieceState(state);
        }

        public void Parse(string value)
        {
            var rows = value.Split(';');
            if (rows.Length != GlobalSettings.FIELD_HEIGHT)
            {
                ErrorLog.Log.WriteLine("Invalid player field: The number of given rows ({0}) does not match the field height ({1}).", rows.Length, GlobalSettings.FIELD_HEIGHT);
                return;
            }
            for (var y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                var cells = rows[y].Split(',');
                if (cells.Length != GlobalSettings.FIELD_WIDTH)
                {
                    ErrorLog.Log.WriteLine("Invalid player field: The number of given cells ({0}) for row {1} does not match the field width ({2}).", rows.Length, y, GlobalSettings.FIELD_WIDTH);
                    return;
                }
                for (var x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                {
                    Grid[x, y] = new FieldCell(this, x, y, (FieldCellType)Enum.Parse(typeof(FieldCellType), cells[x]));

                    // Ignore the piece - we will take care of it by itself later
                    if (Grid[x, y].Type == FieldCellType.Piece)
                    {
                        Grid[x, y].Type = FieldCellType.Empty;
                    }
                }
            }
        }
    }

    class FieldCell
    {
        public Field Field { get; set; }

        public Point Position { get; set; }
        public FieldCellType Type { get; set; }

        public FieldCell(Field field, int x, int y, FieldCellType type)
        {
            Field = field;
            Position = new Point(x, y);
            Type = type;
        }
        public FieldCell(Field field)
        {
            Field = field;
            Position = new Point(0, 0);
            Type = FieldCellType.Empty;
        }

        public bool IsOutOfBounds
        { get { return Position.X >= GlobalSettings.FIELD_WIDTH || Position.X < 0 || Position.Y >= GlobalSettings.FIELD_HEIGHT; } }
        public bool HasCollision
        // Y >= 0 - cells outside of the playground cannot collide
        { get { return Type == FieldCellType.Piece && Field != null && Position.Y >= 0 && (Field.Grid[Position.X, Position.Y].Type == FieldCellType.Block || Field.Grid[Position.X, Position.Y].Type == FieldCellType.Solid); } }

        public FieldCell Clone()
        { return new FieldCell(Field, Position.X, Position.Y, Type); }

        public override string ToString()
        {
            switch (Type)
            {
                case FieldCellType.Empty:
                    return ".";
                case FieldCellType.Piece:
                    return "x";
                case FieldCellType.Block:
                    return "#";
                case FieldCellType.Solid:
                    return "/";
                default:
                    return "/";
            }
        }
    }
}
