﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo.GameNS
{
    class FastPlanSearch
    {
        PieceState Goal { get; set; }
        HashSet<PieceState> Visited { get; set; }
        bool[,] ReachableFromTop { get; set; }
        int HighestY { get; set; }
        public bool HasPlan(Field field)
        {
            
            HighestY = field.GetHighestYOccupied();
            ReachableFromTop = field.GetReachableFromTop();

            bool hasPlan;
            if (HighestY > 1 && IsSkyClear(field))
            {
                hasPlan = true;
            }
            else
            {
                PieceState start = field.GetPieceState();
                Visited = new HashSet<PieceState>();

                // Initialize the piece and move it to reachable state (goal)
                field.SetPieceState(new PieceState(Engine.GetPiecePosition(field.Piece.Type), 0));
                field.PrepareForSearch();
                Goal = field.GetPieceState();

                // Set to start and search
                field.SetPieceState(start);
                hasPlan = SearchPlanFast(field);

                // Return to original state
                field.SetPieceState(start);
            }

            return hasPlan;
        }
        private bool IsSkyClear(Field field)
        {
            foreach (var block in field.Piece.Blocks)
            {
                if (!ReachableFromTop[block.Position.X, block.Position.Y])
                {
                    return false;
                }
            }
            return true;
        }
        private bool SearchPlanFast(Field field)
        {
            PieceState currentState = field.GetPieceState();

            if (Visited.Contains(currentState))
                return false;

            Visited.Add(currentState);

            // Have we found the goal?
            if (currentState == Goal || (HighestY > 1 && IsSkyClear(field)))
            {
                return true;
            }

            List<PieceState> newStates = new List<PieceState>();

            PieceState originalState = field.GetPieceState();

            foreach (var move in PlanSearch.POSSIBLE_MOVES)
            {
                if (field.MakeMove(PlanSearch.GetMove(move)))
                {
                    // Don't add states above our goal
                    if (field.Piece.Position.Y >= Goal.Position.Y)
                        newStates.Add(field.GetPieceState());
                }
                field.BacktrackMove(PlanSearch.GetMove(move));
            }

            newStates.Sort((x, y) => Helpers.ManhattanDistace(x, Goal, field.Piece.RotationCount).CompareTo(Helpers.ManhattanDistace(y, Goal, field.Piece.RotationCount)));

            foreach (var state in newStates)
            {
                field.SetPieceState(state);
                if(SearchPlanFast(field))
                {
                    return true;
                }
            }

            field.SetPieceState(originalState);

            return false;
        }
    }
}
