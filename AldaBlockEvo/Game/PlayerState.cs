﻿using System;
using AldaBlockEvo.OnlineGaming;
using AldaBlockEvo.OnlineGaming.Commands;

namespace AldaBlockEvo.GameNS
{
    class PlayerState : EngineCommandReceiver
    {
        public int RowPoints { get; set; }
        public int Combo { get; set; }
        public Field Field { get; set; }

        public PlayerState()
        {
            Field = new Field();
            RowPoints = 0;
            Combo = 0;

            RouteCommand<PlayerCommand>(ReceiveCommand);
        }

        private PlayerState(Field field, int rowPoints, int combo)
        {
            Field = field;
            RowPoints = rowPoints;
            Combo = combo;

            RouteCommand<PlayerCommand>(ReceiveCommand);
        }

        public void ReceiveCommand(PlayerCommand command)
        {
            switch (command.Key)
            {
                case "row_points":
                    RowPoints = int.Parse(command.Value);
                    break;
                case "combo":
                    Combo = int.Parse(command.Value);
                    break;
                case "field":
                    Field.Parse(command.Value);
                    break;
                default:
                    ErrorLog.Log.WriteLine("Invalid player command: {0}", command.Key);
                    break;
            }
        }

        public PlayerState ShallowClone()
        {
            return new PlayerState(Field, RowPoints, Combo);
        }
    }
}
