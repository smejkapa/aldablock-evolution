﻿using AldaBlockEvo.OnlineGaming;
using AldaBlockEvo.OnlineGaming.Commands;
using System;

namespace AldaBlockEvo.GameNS
{
    class GameState : EngineCommandReceiver
    {
        public int Round { get; set; }
        public PieceType CurrentPiece { get; set; }
        public PieceType NextPiece { get; set; }
        public Point PiecePosition { get; set; }

        public GameState()
        {
            RouteCommand<GameStateCommand>(ReceiveCommand);
        }

        public void ReceiveCommand(GameStateCommand command)
        {
            switch (command.Key)
            {
                case "round":
                    Round = int.Parse(command.Value);
                    break;
                case "this_piece_type":
                    {
                        CurrentPiece = (PieceType)Enum.Parse(typeof(PieceType), command.Value);
                        break;
                    }
                case "next_piece_type":
                    {
                        NextPiece = (PieceType)Enum.Parse(typeof(PieceType), command.Value);
                        break;
                    }
                case "this_piece_position":
                    var parse = (command.Value).Split(',');
                    PiecePosition = new Point(int.Parse(parse[0]), int.Parse(parse[1]));
                    break;
                default:
                    ErrorLog.Log.WriteLine("Invalid game state command: {0}", command.Key);
                    break;
            }
        }
    }
}
