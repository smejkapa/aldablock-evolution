﻿using System;
using AldaBlockEvo.OnlineGaming;
using AldaBlockEvo.OnlineGaming.Commands;

namespace AldaBlockEvo
{
    class MatchSettings : EngineCommandReceiver
    {
        public int MaximumTimeBank { get; private set; }
        public int TimePerMove { get; private set; }
        public string[] PlayerNames { get; private set; }
        /// <summary>
        /// Name of your bot
        /// </summary>
        public string PlayerName { get; private set; }

        public MatchSettings()
        {
            RouteCommand<SettingsCommand>(ProcessCommand);
        }

        public void ProcessCommand(SettingsCommand command)
        {
            switch (command.Key)
            {
                case "timebank":
                    MaximumTimeBank = int.Parse(command.Value);
                    break;
                case "time_per_move":
                    TimePerMove = int.Parse(command.Value);
                    break;
                case "player_names":
                    PlayerNames = (command.Value).Split(',');
                    break;
                case "your_bot":
                    PlayerName = command.Value;
                    break;
                case "field_width":
                    int FieldWidth = int.Parse(command.Value);
                    if (GlobalSettings.FIELD_WIDTH != FieldWidth)
                        throw new InvalidFieldDimensionException(string.Format("Our field width: {0} does not match width: {1} the one given by the on-line engine.",GlobalSettings.FIELD_WIDTH, FieldWidth));
                    break;
                case "field_height":
                    int FieldHeight = int.Parse(command.Value);
                    if (GlobalSettings.FIELD_HEIGHT != FieldHeight)
                        throw new InvalidFieldDimensionException(string.Format("Our field height: {0} does not match height: {1} the one given by the on-line engine.", GlobalSettings.FIELD_HEIGHT, FieldHeight));
                    break;
                default:
                    ErrorLog.Log.WriteLine("Invalid match settings command: {0}", command.Key);
                    break;
            }
        }
        public MatchSettings(int timeBank, int timePerMove, string[] playerNames, string playerName)
        {
            MaximumTimeBank = timeBank;
            TimePerMove = timePerMove;
            PlayerNames = playerNames;
            PlayerName = playerName;
        }
    }
}
