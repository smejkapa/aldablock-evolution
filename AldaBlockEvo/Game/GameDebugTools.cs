﻿//#define PLAY_ONLINE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo.GameNS
{
#if !PLAY_ONLINE
    static class GameDebugTools
    {
        public static string MPFieldFormat(PlanExecutionResult[] results)
        {
            // 3 = previous, current, next
            string[][][] lines = new string[3][][];
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = new string[results.Length][];
            }

            for (int i = 0; i < results.Length; i++)
            {
                lines[0][i] = results[i].PreviousField.Split('\n');
                lines[1][i] = results[i].CurrentField.Split('\n');
                lines[2][i] = results[i].NextField.Split('\n');
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lines.Length; i++)
            {
                // Two player only
                for (int j = 0; j < lines[i][0].Length; j++)
                {
                    sb.Append($"{lines[i][0][j]}\t{lines[i][1][j]}\n");
                }
            }

            return sb.ToString();
        }
    }
#endif
}
