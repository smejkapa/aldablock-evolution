﻿//#define CONSOLE_SP_GRAPHICS
using System;

namespace AldaBlockEvo.GameNS
{
    struct FieldScore
    {
        public int LinesRemoved {get; private set; }
        /// <summary>
        /// Score gained in this round
        /// </summary>
        public int Score {get; private set; }
        public int LastPieceHeight {get; private set; }
        public int Combo { get; set; }
        /// <summary>
        /// There is no score for lost game - the game is just lost
        /// </summary>
        public bool IsLosing {get; private set; }

        public FieldScore(int linesRemoved, int score, int lastPieceHeight, bool isLosing, int combo) : this()
        {
            LinesRemoved = linesRemoved;
            Score = score;
            LastPieceHeight = lastPieceHeight;
            IsLosing = isLosing;
            Combo = combo;
        }
    }
    
    /// <summary>
    /// Contains static methods that represent the rules of the game
    /// e.g. for a given field and a plan get a score and remove lines
    /// </summary>
    static class Engine 
    {
        /// <summary>
        /// Computes a score for a given field.
        /// </summary>
        /// <param name="state">Player state for given field to add combo.</param>
        /// <param name="field">Field to be rated.</param>
        /// <param name="newField">Field after modifications - removed lines etc.</param>
        /// <returns>Score for a field.</returns>
        public static FieldScore GetScore(PlayerState state, Field field, out Field newField)
        {
            // Do not modify the old field - someone may want to use it
            newField = field.Clone();

            // If the piece is not inside the field - the bot has lost the game
            if (newField.IsPieceOut())
            {
                return new FieldScore(0, 0, 21, true, 1);
            }

            // Drop the piece to the field
            newField.ProjectPiece();

            int clearedLines = 0;
            bool perfectClear = true;
            int solidLines = 0;
            for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                // Do not analyze solid lines
                if (newField.Grid[0, y].Type == FieldCellType.Solid)
                {
                    solidLines = GlobalSettings.FIELD_HEIGHT - y;
                    break;
                }

                bool fullLine = true;
                bool fullEmpty = true;
                for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                {
                    // If there is a hole - line is not full and cannot be deleted
                    if (newField.Grid[x, y].Type == FieldCellType.Empty)
                        fullLine = false;
                    else
                        fullEmpty = false;
                }
                if (fullLine)
                    ++clearedLines;
                // Perfect clear = all lines must be homogeneous
                if (!fullEmpty && !fullLine)
                    perfectClear = false;
            }

            int score = 0;
            int lastPieceHeight = GlobalSettings.FIELD_HEIGHT - newField.Piece.Position.Y;
            
            // Do nothing if we did not clear any lines
            if (clearedLines > 0)
            {
                ++state.Combo;
                // How did we clear the lines?
                if (perfectClear)
                {
                    score = GlobalSettings.PERFECT_CLEAR;
                }
                else
                {
                    if (IsPossibleTSpin(newField))
                    {
                        score = GlobalSettings.T_SPIN[clearedLines - 1];
                    }
                    else
                    {
                        score = GlobalSettings.N_LINE_CLEAR[clearedLines - 1];
                    }
                    score += state.Combo - 1;
                }
            }
            else
            {
                state.Combo = 0;
            }

            // Create block cells from where the piece is
            newField.PieceToBlock();
            if (clearedLines > 0)
            {
                RemoveFullLines(newField, clearedLines, solidLines);
            }
            state.RowPoints += score;

            return new FieldScore(clearedLines, score, lastPieceHeight, false, state.Combo);
        }

        private static void RemoveFullLines(Field field, int clearedLines, int solidLines)
        {
            for (int i = 0; i < clearedLines; i++)
            {
                for (int y = 0; y < GlobalSettings.FIELD_HEIGHT- solidLines; y++)
                {
                    bool full = true;
                    for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                    {
                        if (field.Grid[x, y].Type == FieldCellType.Empty)
                        {
                            full = false;
                        }
                    }
                    if (full)
                    {
                        RemoveLine(field, y);
                        break;
                    }
                }
            }
        }
        private static void RemoveLine(Field field, int lineY)
        {
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                for (int y = lineY; y > 0; y--)
                {
                    field.Grid[x, y].Type = field.Grid[x, y - 1].Type;
                }
            }
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                field.Grid[x, 0].Type = FieldCellType.Empty;
            }
        }
        /// <summary>
        /// Detects a possible T spin. There is no guarantee the bot will use it, but we assume it will.
        /// </summary>
        /// <param name="field">Field with the piece we want the T spin detect for.</param>
        /// <returns>If the T spin is possible with this piece in this position.</returns>
        private static bool IsPossibleTSpin(Field field)
        {
            if (field.Piece.Type != PieceType.T)
                return false;
            /* 
            Original rules:
                T-spins are detected by checking if the last move was a turn left or turn right, 
                and 3 out of 4 corners of the T shape bounding box are occupied with blocks in the field. 
            Our detection:
                3 out of 4 corners of the T shape bounding box are occupied with blocks.
                4 out of 4 "middle cells" (a cell between two corners) are empty.
            */


            int fullCorners = 0;
            int emptyMiddles = 0;
            Point topLeft = field.Piece.Position;
            for (int xAdd = 0; xAdd < 2; xAdd++)
            {
                for (int yAdd = 0; yAdd < 2; yAdd++)
                {
                    int fullY = topLeft.Y + yAdd * 2;
                    int fullX = topLeft.X + xAdd * 2;

                    // If we are against the wall there is no spin actually
                    if (fullY < 0 || fullY >= GlobalSettings.FIELD_HEIGHT ||
                        fullX < 0 || fullX >= GlobalSettings.FIELD_WIDTH)
                        return false;

                    if (field.GetCellType(new Point(fullX, fullY)) == FieldCellType.Block)
                    {
                        ++fullCorners;
                    }

                    int emptyY;
                    int emptyX;
                    if (yAdd == 1) // Middle row - get 2 cells - at the beginning and at the end
                    {
                        emptyY = topLeft.Y + 1;
                        emptyX = topLeft.X + xAdd * 2;
                    }
                    else // Middle column - get 2 cells - at the top and at the bottom
                    {
                        emptyY = topLeft.Y + yAdd * 2;
                        emptyX = topLeft.X + 1;
                    }

                    if (field.GetCellType(new Point(emptyX, emptyY)) == FieldCellType.Empty)
                    {
                        ++emptyMiddles;
                    }
                }
            }

            return fullCorners == 3 && emptyMiddles == 4;
        }

        private static int CountGarbageLines(int previousScore, int currentScore)
        {
            // How many points did we have when we added the last g line? e.g. we have 5 points -> we added g line for 4 points
            int lastGLineAdd = previousScore - (previousScore % GlobalSettings.SCORE_PER_GARBAGE_LINE);
            // How many points do we have left over from the last addition + how many points we added during this round
            int scoreToGLines = currentScore - lastGLineAdd;

            // Every k row points in the leftover score = 1 garbage line
            return scoreToGLines / GlobalSettings.SCORE_PER_GARBAGE_LINE;
        }

        /// <summary>
        /// Performs given move on given field. Behaves due to rules when dealing with impossible moves.
        /// </summary>
        /// <param name="move">Move to perform.</param>
        /// <param name="field">Filed to modify.</param>
        private static void MakeMove(Move move, Field field)
        {
            if (!field.MakeMove(move))
            {
                field.BacktrackMove(move);
                // Skip all moves except for left/right - try to perform down in this case
                if (move == Move.Left || move == Move.Right)
                {
                    MakeMove(Move.Down, field);
                }
                // There is a bug in the bot if this happens
                throw new InvalidMoveException(string.Format("Invalid move {0} passed by a bot.", move.ToString()));
            }
        }
        /// <summary>
        /// Executes given set of moves on given field and determines if we can continue the game with this field
        /// </summary>
        /// <param name="state">State at which given player is.</param>
        /// <param name="plan">Plan to execute from given state</param>
        /// <returns>False if the plan leads to endgame state, true otherwise.</returns>
#if PLAY_SP
        public static PlanExecutionResult ExecutePlan(PlayerState state, Plan plan)
#else
        public static PlanExecutionResult ExecutePlan(PlayerState state, Plan plan)
#endif
        {
#if CONSOLE_SP_GRAPHICS
            {
                //Console.WriteLine(state.Field);

/*                  Console.Write("Show state generation? a/n : ");
                    bool v = Console.ReadLine() == "a";
                    var states = state.Field.GenerateAllGroundedStates();

                    foreach (var s in states)
                    {
                        Console.WriteLine("X: "+ s.Position.X+ " Y: "+ s.Position.Y+ " Rot: "+ s.Rotation);
                    }
*/   
                

                //Console.ReadLine();
            }
#endif
#if CONSOLE_MP_GRAPHICS
            string prevField = state.Field.ToString();
#else
            string prevField = "";
#endif
            // Alternate the field according to the plan
            foreach (var move in plan.Moves)
            {
                MakeMove(move, state.Field);
#if CONSOLE_SP_GRAPHICS
                /*Console.WriteLine(state.Field);
                Console.ReadLine();*/
#endif
            }

            int prevScore = state.RowPoints;
#if CONSOLE_MP_GRAPHICS
            string currField = state.Field.ToString();
#else
            string currField = "";
#endif

            Field newField;
            // Rate the field
            FieldScore score = GetScore(state, state.Field, out newField);
            state.Field = newField;

#if CONSOLE_SP_GRAPHICS
            {
                Console.WriteLine($"Combo: {state.Combo}, Score: {state.RowPoints}, CurrentScore: {score.Score}, IsLosing: {score.IsLosing}");
            }
#endif

            return new PlanExecutionResult(!score.IsLosing, CountGarbageLines(prevScore, state.RowPoints), prevField, currField);
        }
        public static Point GetPiecePosition(PieceType piece)
        {
            switch (piece)
            {
                case PieceType.I:
                    return new Point(3, -1);
                case PieceType.J:
                    return new Point(3, -1);
                case PieceType.L:
                    return new Point(3, -1);
                case PieceType.O:
                    return new Point(4, -1);
                case PieceType.S:
                    return new Point(3, -1);
                case PieceType.T:
                    return new Point(3, -1);
                case PieceType.Z:
                    return new Point(3, -1);
                default:
                    throw new ArgumentOutOfRangeException("Unknown piece.");
            }
        }
        /// <summary>
        /// Adds static lines, garbage lines and sets the new piece.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="round"></param>
        /// <param name="nextPiece"></param>
        /// <returns>True if the game may continue with this field, false otherwise (game should end).</returns>
        public static bool PrepareForNextRound(Field field, int round, PieceType nextPiece, int gLinesToAdd)
        {
            bool addLineScucess = true;
            // Every 20th round (starting from 21st)
            if (round > 1 && round % GlobalSettings.STATIC_LINE_ADD_RATE == 1)
            {
                addLineScucess = field.AddStaticLine();
            }

            for (int i = 0; i < gLinesToAdd; i++)
            {
                addLineScucess = addLineScucess && field.AddGarbageLine();
            }

#if PLAY_SP
            if (round > 1 && round % GlobalSettings.GARBAGE_LINE_ADD_RATE == 1)
            {
                addLineScucess = addLineScucess && field.AddGarbageLine();
            }
#endif

            // Combo and RowPoints are updated after a plan has been executed
            field.Piece = new Piece(nextPiece, field, GetPiecePosition(nextPiece));

            bool pieceCollides = field.Piece.IsColliding();

            // Was the modification successful?
            return addLineScucess && !pieceCollides;
        }
    }
}
