﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo.GameNS
{
    class PlanExecutionResult
    {
        public bool ExecutionSuccess { get; private set; }
        public int GarbageLineCount { get; private set; }
        public string PreviousField { get; private set; }
        public string CurrentField { get; private set; }
        public string NextField { get; set; }
        public PlanExecutionResult()
        {
        }
        public PlanExecutionResult(bool success, int garbageLineCout, string previousField, string currentField)
        {
            ExecutionSuccess = success;
            GarbageLineCount = garbageLineCout;
            PreviousField = previousField;
            CurrentField = currentField;
        }
    }
}
