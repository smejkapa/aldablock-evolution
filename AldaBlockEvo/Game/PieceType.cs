﻿namespace AldaBlockEvo
{
    public enum PieceType : byte
    {
        I,
        J,
        L,
        O,
        S,
        T,
        Z
    }
}
