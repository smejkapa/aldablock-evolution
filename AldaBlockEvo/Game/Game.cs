﻿//#define PLAY_ONLINE

using System;
using System.Diagnostics;
using System.IO;
using AldaBlockEvo.GameNS;
using AldaBlockEvo;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;

#if !PLAY_ONLINE
using static AldaBlockEvo.GlobalSettings;
#endif

namespace AldaBlockEvo.GameNS
{
#if !PLAY_ONLINE
    interface IGame
    {
        /// <summary>
        /// Starts and plays through the whole game
        /// </summary>
        /// <returns>The resulting score</returns>
        GameResult StartGame(int seed, LinkedList<PieceType> pieceSequence = null);
    }

    class Game : IGame
    {
        GameState GameState;
        List<PlayerState> PlayerStates;

        LinkedList<PieceType> PieceSequence;

        Random Rnd;
        int Seed;

        public Game(IBot[] bots)
        {
            Bots = bots;
        }
        public IBot[] Bots { get; }
        private string[] BotNames;


        private void SetupBots()
        {
            BotNames = new string[Bots.Length];
            for (int i = 0; i < Bots.Length; i++)
            {
                BotNames[i] = BOT_NAME + $"_{i}";
            }
            for (int i = 0; i < Bots.Length; i++)
            {
                var settings = new MatchSettings
                (
                    MAX_TIME_BANK,
                    TIME_PER_MOVE,
                    BotNames,
                    BotNames[i]
                );
                Bots[i].SetSettings(settings);
            }
        }

        private PieceType GetRandomPiece()
            => (PieceType)Rnd.Next(Enum.GetValues(typeof(PieceType)).Length);

        private void InitGame()
        {
            GameState = new GameState();
            GameState.Round = 0;
            if (PieceSequence?.First != null)
            {
                GameState.NextPiece = PieceSequence.First.Value;
                PieceSequence.RemoveFirst();
            }
            else
            {
                GameState.NextPiece = GetRandomPiece();
            }

            PlayerStates = new List<PlayerState>();

            // Game state is the same for all bots
            for (int i = 0; i < Bots.Length; i++)
            {
                PlayerStates.Add(new PlayerState());
            }
        }

        private void UpdateGameState()
        {
            ++GameState.Round;
            GameState.CurrentPiece = GameState.NextPiece;
            if (PieceSequence?.First != null)
            {
                GameState.NextPiece = PieceSequence.First.Value;
                PieceSequence.RemoveFirst();
            }
            else
            {
                GameState.NextPiece = GetRandomPiece();
            }
            GameState.PiecePosition = Engine.GetPiecePosition(GameState.CurrentPiece);

            for (int i = 0; i < Bots.Length; i++)
            {
                Bots[i].UpdateGameState(GameState);
            }
        }
        private List<bool> UpdatePlayerState(PlanExecutionResult[] executions)
        {
            List<bool> updates = new List<bool>();
            for (int i = 0; i < Bots.Length; i++)
            {
                updates.Add(Engine.PrepareForNextRound(
                    PlayerStates[i].Field,
                    GameState.Round,
                    GameState.CurrentPiece,
#if PLAY_SP
                    0 // There is no one to add g lines from/to
#else
                    executions[(i + 1) % 2].GarbageLineCount // Only two player games make sense
#endif
                    ));
            }

            // Only update if all updates were successful
            if (!updates.Contains(false))
            {
                // Tell bots about each other
                for (int i = 0; i < Bots.Length; i++)
                {
                    for (int j = 0; j < Bots.Length; j++)
                    {
                        Bots[i].UpdatePlayerState(BotNames[j], PlayerStates[j]);
                    }
                }
            }

#if CONSOLE_MP_GRAPHICS
            if (GameState.Round > 1)
            {
                Console.WriteLine();
                for (int i = 0; i < Bots.Length; i++)
                {
                    executions[i].NextField = PlayerStates[i].Field.ToString();
                }
                Console.WriteLine(GameDebugTools.MPFieldFormat(executions));
                Console.ReadLine();
            }
            Console.WriteLine($"Round: {GameState.Round} CurrentPiece: {GameState.CurrentPiece} NextPiece: {GameState.NextPiece}");
            for (int i = 0; i < Bots.Length; i++)
            {
                Console.Write($"Bot {i + 1} - Score: {PlayerStates[i].RowPoints} Combo: {PlayerStates[i].Combo}\t");
            }
#endif

            return updates;
        }

        /// <summary>
        /// Plays a game for a single bot
        /// </summary>
        /// <returns>Result of the game.</returns>
        public GameResult StartGame(int seed, LinkedList<PieceType> pieceSequence = null)
        {
            Rnd = new Random(seed);
            Seed = seed;
            PieceSequence = pieceSequence;

            List<bool> continueGame;

#if EVOLUTION
            try
            {
#endif
            InitGame();
            // Setup the bot
            SetupBots();

#if FILE_LOG
            StreamWriter gameLog = new StreamWriter(GlobalSettings.BASE_LOG_DIR + @"\Games\" + GlobalSettings.DATE_TIME_FILE_FORMAT + ".txt");
#endif

            Stopwatch timeToMove = new Stopwatch();
#if PLAY_SP
            double worstRoundTime = 0;
#endif

            PlanExecutionResult[] executions = new PlanExecutionResult[Bots.Length];
            for (int i = 0; i < executions.Length; i++)
            {
                executions[i] = new PlanExecutionResult();
            }

            // Game running loop
            do
            {
                // Update game state - same for both bots
                UpdateGameState();

                // Update player state - points, combo, field...
                if ((continueGame = UpdatePlayerState(executions)).Contains(false))
                    break;

                for (int i = 0; i < Bots.Length; i++)
                {
                    #region Logging
#if CONSOLE_LOG || FILE_LOG
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("###################################");
                    sb.AppendLine($"### Round: {GameState.Round}, Piece: {GameState.CurrentPiece}, Next: {GameState.NextPiece}");
                    sb.AppendLine("###################################");
                    sb.AppendLine(PlayerStates[i].Field.ToString());
#if FILE_LOG
                    gameLog.WriteLine(sb);
#elif CONSOLE_LOG
                    Console.WriteLine(sb.ToString());
#endif
#endif
                    #endregion
                    timeToMove.Restart();
                    // Ask the bot for moves and execute them
                    Plan moves = Bots[i].GetMoves(MAX_TIME_BANK); // Do not worry about time ATM
                    timeToMove.Stop();
#if PLAY_SP
                    if (timeToMove.Elapsed.TotalMilliseconds > worstRoundTime)
                        worstRoundTime = timeToMove.Elapsed.TotalMilliseconds;
#endif
#if CONSOLE_LOG
                    Console.WriteLine($"--- Moves found in {timeToMove.Elapsed.TotalMilliseconds}ms");
                    //Console.ReadLine();
#endif
                    executions[i] = Engine.ExecutePlan(PlayerStates[i], moves);
                    continueGame[i] = executions[i].ExecutionSuccess;
                }
            } while (!continueGame.Contains(false));
#if EVOLUTION
            }
            #region Catch
            catch
            {
                StreamWriter sw = new StreamWriter($@"{BASE_LOG_DIR}exception-[{DATE_TIME_FILE_FORMAT}]({Thread.CurrentThread.ManagedThreadId})_{Seed}.txt");
                sw.WriteLine($"Seed: {Seed}");
                for (int i = 0; i < Bots.Length; i++)
                {
                    sw.WriteLine(Bots[i].GetWeights);
                }
                sw.Close();

                List<int> score = new List<int>();
                for (int i = 0; i < Bots.Length; i++)
                {
                    score.Add(0);
                }
                return new GameResult(Winner.Draw, 0, score);
            }
            #endregion
#endif
#if FILE_LOG
            gameLog.Close();
#endif

#if PLAY_SP
            Console.WriteLine($"Longest round took: {worstRoundTime}");
            Console.WriteLine($"Avg state generated: {Bot.validStates.Average()}");
            Console.WriteLine($"Min state generated: {Bot.validStates.Min()}");
            Console.WriteLine($"Max state generated: {Bot.validStates.Max()}");
#endif
            // Someone has bot has lost the game - end and out
            return new GameResult
            (
            (Winner)continueGame.FindIndex(x => x == true), // Find the one who could continue, -1 if none
            GameState.Round,
            PlayerStates.Select(x => x.RowPoints).ToList()
            );
        }
    }
#endif
        }
