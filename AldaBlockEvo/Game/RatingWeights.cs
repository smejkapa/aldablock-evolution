﻿//#define PLAY_ONLINE

using System.Globalization;
using System.IO;
using AldaBlockEvo.GameNS;

namespace AldaBlockEvo
{
    struct WeightTuple
    {
        public int Sigma { get; set; }
        public double Exponent { get; set; }
        public WeightTuple(int sigma, double exponent) : this()
        {
            Sigma = sigma;
            Exponent = exponent;
        }
#if !PLAY_ONLINE
        public override string ToString() 
            => string.Format($"({Sigma}; {Exponent})", CultureInfo.InvariantCulture);
#endif
    }

    /// <summary>
    ///  Contains all weights for currently used rating function
    /// </summary>
    class RatingWeights
    {
        public WeightTuple[] Ratings {get; private set; }
        public RatingWeights()
        {
            Ratings = new WeightTuple[(int)CriteriaNames.CriteriaCount];
        }
        public RatingWeights(string weights)
        {
            Ratings = new WeightTuple[(int)CriteriaNames.CriteriaCount];
            ReadFromString(weights);
        }
        public RatingWeights DeepClone()
        {
            RatingWeights clone = new RatingWeights();
            for (int i = 0; i < Ratings.Length; i++)
            {
                clone.Ratings[i] = Ratings[i];
            }
            return clone;
        }
        public override string ToString()
        { return string.Join(",", Ratings); }

        public void ReadFromFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            string line = sr.ReadLine();
            ReadFromString(line);
        }
        public void ReadFromString(string weights)
        {
            var split = weights.Split(',');
            for (int i = 0; i < split.Length; i++)
            {
                var tuple = split[i].Split(';');
                Ratings[i].Sigma = int.Parse(tuple[0].Trim('('));
                Ratings[i].Exponent = double.Parse(tuple[1].Trim(')'), CultureInfo.InvariantCulture);
            }
        }
    }
}
