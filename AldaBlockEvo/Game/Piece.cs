﻿using System;
using System.Collections.Generic;

namespace AldaBlockEvo.GameNS
{
    struct Point : IEquatable<Point>
    {
        public Point(int x, int y) : this()
        {
            X = x;
            Y = y;
        }
        public int X {get; private set; }
        public int Y {get; private set; }

        public bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            unchecked
            {
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
            }
            return hash;
        }


        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point operator -(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }

    }

    enum Direction : short
    {
        Left = -1,
        Right = 1
    }

    class Piece
    {
        public PieceType Type { get; private set; }
        private int Size;
        /// <summary>
        /// The whole shape matrix including empty cells
        /// </summary>
        public FieldCell[][] Shape { get; private set; }
        /// <summary>
        /// Only cells occupied by the piece itself - no empty cells
        /// </summary>
        public FieldCell[] Blocks { get; private set; }
        public Point Position { get; private set; }
        private Field Field { get; set; }
        public int Rotation { get; private set; }
        public int RotationCount { get; private set; }

        public Piece Clone()
        {
            Piece clone = new Piece(Type, Field, Position);
            while (clone.Rotation != Rotation)
            {
                clone.TurnRight();
            }

            return clone;
        }
        public void ChangeField(Field field)
        {
            Field = field;
            for (int x = 0; x < Shape.Length; x++)
            {
                for (int y = 0; y < Shape[x].Length; y++)
                {
                    Shape[x][y].Field = field;
                }
            }
        }

        public Piece(PieceType type, Field field, Point position)
        {
            Type = type;
            Field = field;
            Blocks = new FieldCell[4];
            Position = position;
            Rotation = 0;

            SetShape();
            SetBlockLocations();
        }

        /// <summary>
        /// Turns the piece 90 degrees right
        /// </summary>
        public void TurnRight()
        {
            FieldCell[][] tmp = Transpose();
            for (int x = 0; x < Size; x++)
            {
                Shape[x] = tmp[Size - x - 1];
            }

            Rotation = ++Rotation % RotationCount;

            SetBlockLocations();
        }
        /// <summary>
        /// Turns the piece 90 degrees left
        /// </summary>
        public void TurnLeft()
        {
            int i = 0;
            FieldCell[][] tmp = Transpose();
            for (int y = 0; y < Size; y++)
            {
                for (int x = 0; x < Size; x++)
                {
                    Shape[x][y] = tmp[x][Size - y - 1];
                    if (Shape[x][y].Type == FieldCellType.Piece)
                    {
                        Blocks[i] = Shape[x][y];
                        ++i;
                    }
                }
            }

            --Rotation;
            // Manual modulo
            if (Rotation < 0)
            {
                Rotation += RotationCount;
            }

            SetBlockLocations();
        }
        /// <summary>
        /// Moves the piece down by one cell
        /// </summary>
        public void OneDown()
        {
            Position = new Point(Position.X, Position.Y + 1);
            SetBlockLocations();
        }
        /// <summary>
        ///  Moves the piece up by n cells
        /// </summary>
        /// <param name="n">How many cells to move the piece</param>
        public void NUp(int n)
        {
            Position = new Point(Position.X, Position.Y - n);
            SetBlockLocations();
        }
        /// <summary>
        /// Drops the piece to the field
        /// </summary>
        /// <returns>How many down moves were performed</returns>
        public byte Drop()
        {
            byte downMoves = 0;
            // We assume the piece is in consistent state and currently not colliding && in bounds
            do
            {
                ++downMoves;
                OneDown();
            } while (!IsOutOfBounds() && !IsColliding());

            // Piece collides -> move it to the last non colliding state
            NUp(1);
            --downMoves;

            return downMoves;
        }

        /// <summary>
        /// Move by one square in given direction
        /// </summary>
        /// <param name="direction">Direction to move at - left or right</param>
        public void Move(Direction direction)
        {
            Position = new Point(Position.X + (int)direction, Position.Y);
            SetBlockLocations();
        }

        public void SetPieceState(PieceState state)
        {
            Position = state.Position;
            while(Rotation != state.Rotation)
            {
                TurnRight();
            }
            SetBlockLocations();
        }

        public void SetBlockPosition(int index, Point position)
        {
            Position += position - Blocks[index].Position;
            SetBlockLocations();
        }

        private FieldCell[][] Transpose()
        {
            FieldCell[][] tmp = new FieldCell[Size][];
            for (int y = 0; y < Size; y++)
            {
                tmp[y] = new FieldCell[Size];
                for (int x = 0; x < Size; x++)
                {
                    tmp[y][x] = Shape[x][y];
                }
            }
            return tmp;
        }

        public bool IsOutOfBounds()
        {
            for (int y = 0; y < Size; y++)
            {
                for (int x = 0; x < Size; x++)
                {
                    if (Shape[x][y].IsOutOfBounds)
                        return true;
                }
            }
            return false;
        }

        public bool IsColliding()
        {
            for (int y = 0; y < Size; y++)
            {
                for (int x = 0; x < Size; x++)
                {
                    if (Shape[x][y].HasCollision)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Sets the actual position on the field.
        /// </summary>
        private void SetBlockLocations()
        {
            for (int y = 0; y < Size; y++)
            {
                for (int x = 0; x < Size; x++)
                {
                    if (Shape[x][y].Type == FieldCellType.Piece)
                    {
                        Shape[x][y].Position = new Point(x + Position.X, y + Position.Y);
                    }
                }
            }
        }
        private void SetShape()
        {
            switch (Type)
            {
                case PieceType.I:
                    Size = 4;
                    RotationCount = 4;
                    Shape = initializeShape();
                    Blocks[0] = Shape[0][1];
                    Blocks[1] = Shape[1][1];
                    Blocks[2] = Shape[2][1];
                    Blocks[3] = Shape[3][1];
                    break;
                case PieceType.J:
                    Size = 3;
                    RotationCount = 4;
                    Shape = initializeShape();
                    Blocks[0] = Shape[0][0];
                    Blocks[1] = Shape[0][1];
                    Blocks[2] = Shape[1][1];
                    Blocks[3] = Shape[2][1];
                    break;
                case PieceType.L:
                    Size = 3;
                    RotationCount = 4;
                    Shape = initializeShape();
                    Blocks[0] = Shape[2][0];
                    Blocks[1] = Shape[0][1];
                    Blocks[2] = Shape[1][1];
                    Blocks[3] = Shape[2][1];
                    break;
                case PieceType.O:
                    Size = 2;
                    RotationCount = 1;
                    Shape = initializeShape();
                    Blocks[0] = Shape[0][0];
                    Blocks[1] = Shape[1][0];
                    Blocks[2] = Shape[0][1];
                    Blocks[3] = Shape[1][1];
                    break;
                case PieceType.S:
                    Size = 3;
                    RotationCount = 4; // 2?
                    Shape = initializeShape();
                    Blocks[0] = Shape[1][0];
                    Blocks[1] = Shape[2][0];
                    Blocks[2] = Shape[0][1];
                    Blocks[3] = Shape[1][1];
                    break;
                case PieceType.T:
                    Size = 3;
                    RotationCount = 4;
                    Shape = initializeShape();
                    Blocks[0] = Shape[1][0];
                    Blocks[1] = Shape[0][1];
                    Blocks[2] = Shape[1][1];
                    Blocks[3] = Shape[2][1];
                    break;
                case PieceType.Z:
                    Size = 3;
                    RotationCount = 4; // 2?
                    Shape = initializeShape();
                    Blocks[0] = Shape[0][0];
                    Blocks[1] = Shape[1][0];
                    Blocks[2] = Shape[1][1];
                    Blocks[3] = Shape[2][1];
                    break;
            }

            // set type to Piece
            for (int i = 0; i < Blocks.Length; i++)
            {
                Blocks[i].Type = FieldCellType.Piece;
            }
        }
        private FieldCell[][] initializeShape()
        {
            FieldCell[][] newShape = new FieldCell[Size][];
            for (int x = 0; x < Size; x++)
            {
                newShape[x] = new FieldCell[Size];
                for (int y = 0; y < Size; y++)
                {
                    newShape[x][y] = new FieldCell(Field);
                }
            }
            return newShape;
        }
        public int GetLowestYPossible()
        {
            return Position.Y + Size - 1;
        }

        private int BlockSort(FieldCell a, FieldCell b)
        {
            if (a.Position.X < b.Position.X)
                return 1;
            else if (a.Position.X > b.Position.X)
                return -1;
            else if (a.Position.Y < b.Position.Y)
                return 1;
            else if (a.Position.Y > b.Position.Y)
                return -1;

            throw new InvalidPieceStateException("There cannot be two identical blocks");
        }

        public long GetBlocksCode()
        {
            // Sort blocks
            FieldCell[] blocksSorted = (FieldCell[])Blocks.Clone();
            Array.Sort(blocksSorted, BlockSort);

            // Compute the code
            long value = blocksSorted[0].Position.X;
            value = (value << 4) + blocksSorted[1].Position.X;
            value = (value << 4) + blocksSorted[2].Position.X;
            value = (value << 4) + blocksSorted[3].Position.X;

            value = (value << 5) + blocksSorted[0].Position.Y;
            value = (value << 5) + blocksSorted[1].Position.Y;
            value = (value << 5) + blocksSorted[2].Position.Y;
            value = (value << 5) + blocksSorted[3].Position.Y;

            return value;
        }
    }
}
