﻿//#define PLAY_ONLINE
using System.Collections.Generic;

namespace AldaBlockEvo
{
    enum Move : byte
    {
        Drop,
        Down,
        Left,
        Right,
        TurnLeft,
        TurnRight
    }

    class Plan
    {
        public LinkedList<Move> Moves {get; private set; }
        public double Rating {get; private set; }

        public Plan()
        {
            Moves = new LinkedList<Move>();
            Rating = double.MinValue;
        }
        public Plan(LinkedList<Move> moves, double rating)
        {
            Moves = new LinkedList<Move>(moves);
            Rating = rating;
        }

        public Move GetLastNonDrop()
        {
            if (Moves.Count == 0)
                return Move.Drop;

            var last = Moves.Last;
            while(last != null && last.Value == Move.Drop)
            {
                last = last.Previous;
            }

            return last == null ? Move.Drop : last.Value;
        }

#if !PLAY_ONLINE
        public override string ToString() 
            => $"Score: {Rating} {string.Join(",", Moves)}";
#endif

    }
}
