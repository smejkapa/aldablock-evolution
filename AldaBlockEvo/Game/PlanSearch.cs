﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;

namespace AldaBlockEvo.GameNS
{
    class PlanSearch
    {
        public enum PlanSearchMove
        {
            Up,
            Left,
            Right,
            TurnLeft,
            TurnRight
        }

        const int MAX_NODES = 256;
        public static readonly PlanSearchMove[] POSSIBLE_MOVES = (PlanSearchMove[])Enum.GetValues(typeof(PlanSearchMove));

        private PieceState Start { get; set; }
        private PieceState Goal { get; set; }
        private Field Field { get; set; }

        private class PlanSearchState : PriorityQueueNode, IEquatable<PlanSearchState>
        {
            public PieceState PieceState { get; private set; }
            public int g { get; set; }
            public PlanSearchMove Action { get; set; }

            public PlanSearchState(PieceState state, int g, PlanSearchMove action)
            {
                PieceState = state;
                this.g = g;
                Action = action;
            }

            public bool Equals(PlanSearchState other)
            {
                return PieceState == other.PieceState;
            }
            public override int GetHashCode()
            {
                return PieceState.GetHashCode();
            }
        }

        public PlanSearch(PieceState start, PieceState goal, Field field)
        {
            Start = start;
            Goal = goal;
            Field = field;
        }

        /// <summary>
        /// Minimum moves needed to get to current position. Manhattan metric from goal position. Plus how off are with current rotation.
        /// </summary>
        /// <returns></returns>
        private int h(PieceState current)
        {
            return (Math.Abs(current.Position.X - Goal.Position.X)
                + Math.Abs(current.Position.Y - Goal.Position.Y)
                + Math.Min(Math.Abs(current.Rotation - Goal.Rotation), Field.Piece.RotationCount - Math.Abs(current.Rotation - Goal.Rotation)));
        }
        private int f(PlanSearchState node)
        {
            return node.g + h(node.PieceState);
        }

        /// <summary>
        /// A* search f(s) = h(s) + g(s).
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        public Plan GetPlan()
        {
            Field.SetPieceState(Start);
            PlanSearchState startNode = new PlanSearchState(Start, 0, PlanSearchMove.Left); // The move here does not matter

            HeapPriorityQueue<PlanSearchState> opened = new HeapPriorityQueue<PlanSearchState>(MAX_NODES);
            opened.Enqueue(startNode, f(startNode));

            Dictionary<PieceState, int> shortestPaths = new Dictionary<PieceState, int>();
            shortestPaths.Add(Start, startNode.g);

            Dictionary<PieceState, PlanSearchState> enqueued = new Dictionary<PieceState, PlanSearchState>();
            enqueued.Add(startNode.PieceState, startNode);

            HashSet<PieceState> closed = new HashSet<PieceState>();
            Dictionary<PlanSearchState, PlanSearchState> preceding = new Dictionary<PlanSearchState, PlanSearchState>();

            while(opened.Count > 0)
            {
                var currentNode = opened.Dequeue();

                // We have found the goal, now just generate the plan
                if (currentNode.PieceState == Goal)
                {
                    return GenerateActualPlan(preceding, currentNode);
                }

                closed.Add(currentNode.PieceState);

                // Expand current node
                List<PlanSearchState> adjacentNodes = GenerateAdjacent(currentNode);
                // The queue does not resize itself
                if (opened.Count + adjacentNodes.Count > opened.MaxSize)
                {
#if CONSOLE_LOG
                    Console.WriteLine($"Increasing Q size to {opened.MaxSize + MAX_NODES}");
#endif
                    opened.Resize(opened.MaxSize + MAX_NODES);
                }

                foreach (var newNode in adjacentNodes)
                {
                    // No not reopen already closed nodes
                    if (closed.Contains(newNode.PieceState))
                        continue;

                    if(newNode.PieceState.Position.Y < Goal.Position.Y)
                    {
                        closed.Add(newNode.PieceState);
                        continue;
                    }

                    // We already have this node opened
                    if (shortestPaths.ContainsKey(newNode.PieceState))
                    {
                        // New path is shorter
                        if (shortestPaths[newNode.PieceState] > newNode.g)
                        {
                            shortestPaths[newNode.PieceState] = newNode.g;
                            preceding.Remove(newNode);
                            preceding.Add(newNode, currentNode);
                            var enq = enqueued[newNode.PieceState];
                            enq.g = newNode.g;
                            enq.Action = newNode.Action;
                            opened.UpdatePriority(enq, f(enq));
                        }
                    }
                    else // New state
                    {
                        shortestPaths[newNode.PieceState] = newNode.g;
                        preceding.Add(newNode, currentNode);
                        opened.Enqueue(newNode, f(newNode));
                        enqueued.Add(newNode.PieceState, newNode);
                    }
                }
            }
            // No plan found
            return null;
        }

        private List<PlanSearchState> GenerateAdjacent(PlanSearchState node)
        { 
            List<PlanSearchState> adjacent = new List<PlanSearchState>();
            Field.SetPieceState(node.PieceState);
            foreach (var move in POSSIBLE_MOVES)
            {
                if (Field.MakeMove(GetMove(move)))
                {
                    PieceState state = Field.GetPieceState();
                    adjacent.Add(new PlanSearchState(state, node.g + 1, move));
                }
                Field.BacktrackMove(GetMove(move));
            }
            return adjacent;
        }

        private Plan GenerateActualPlan(Dictionary<PlanSearchState, PlanSearchState> preceding, PlanSearchState goalNode)
        {
            var current = goalNode;
            Plan plan = new Plan();
            // If start == goal we return empty plan
            while (current.PieceState != Start)
            {
                plan.Moves.AddLast(GetReverseMove(current.Action));
                current = preceding[current];
            }
            return plan;
        }
        private Move GetReverseMove(PlanSearchMove move)
        {
            switch (move)
            { 
                case PlanSearchMove.Up:
                    return Move.Down;
                case PlanSearchMove.Left:
                    return Move.Right;
                case PlanSearchMove.Right:
                    return Move.Left;
                case PlanSearchMove.TurnLeft:
                    return Move.TurnRight;
                case PlanSearchMove.TurnRight:
                    return Move.TurnLeft;
                default:
                    throw new InvalidMoveException("Plan search generated impossible move.");
            }
        }
        public static AllMoves GetMove(PlanSearchMove move)
        {
            switch (move)
            {
                case PlanSearchMove.Up:
                    return AllMoves.Up;
                case PlanSearchMove.Left:
                    return AllMoves.Left;
                case PlanSearchMove.Right:
                    return AllMoves.Right;
                case PlanSearchMove.TurnLeft:
                    return AllMoves.TurnLeft;
                case PlanSearchMove.TurnRight:
                    return AllMoves.TurnRight;
                default:
                    return AllMoves.None;
            }
        }
    }
}
