﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
#if PLAY_SP
using System.Linq;
#endif

namespace AldaBlockEvo.GameNS
{
    interface IBot
    {
        void SetSettings(MatchSettings settings);
        /// <summary>
        /// Returns a plan (list of moves) for the next round
        /// </summary>
        /// <param name="miliseconds">Time left for computation</param>
        /// <returns>Plan for the next round</returns>
        Plan GetMoves(int miliseconds);
        void UpdateGameState(GameState state);
        void UpdatePlayerState(string name, PlayerState state);
        RatingWeights GetWeights {get; set; }
    }

    class Bot : IBot
    {
        public RatingWeights Weights {get; private set; }
        public MatchSettings MatchSettings { get; private set; }
        public GameState GameState { get; private set; }
        public Dictionary<string, PlayerState> Players {get; private set; }
        private Field MyField
        { get { return Players[MatchSettings.PlayerName].Field; } }
        private PlayerState MyState
        { get { return Players[MatchSettings.PlayerName]; } }

        Plan BestPlan;

        FastPlanSearch FastPlanSearch { get; set; }

        public Bot(RatingWeights weights)
        {
            Weights = weights;
            Players = new Dictionary<string, PlayerState>();
            MatchSettings = new MatchSettings();
            GameState = new GameState();
            FastPlanSearch = new FastPlanSearch();
        }

        RatingWeights IBot.GetWeights
        {
            get { return Weights; }
            set { }
        }
        
        /// <summary>
        /// Rates given field without modifying it
        /// </summary>
        /// <param name="field">A field to rate</param>
        /// <returns>Rating for given field</returns>
        private double RateField(Field field, FieldScore score)
        {
            // If this field loses us the game - do not use it
            if (score.IsLosing)
            {
                return double.MinValue;
            }

            FieldAnalysis fa = FieldAnalyzer.AnalyzeField(field, score);

            double rating = 0;
            for (int i = 0; i < Weights.Ratings.Length; i++)
            {
                rating += Weights.Ratings[i].Sigma * Math.Pow(fa.Criteria[i], Weights.Ratings[i].Exponent);
            }

            return rating;
        }
        private double RateScore(int score)
        { return Weights.Ratings[GlobalSettings.SCORE_RATING_IDX].Sigma * Math.Pow(score, Weights.Ratings[GlobalSettings.SCORE_RATING_IDX].Exponent); }

        void IBot.SetSettings(MatchSettings settings)
        {
            MatchSettings = settings;
        }

        private class SearchState : IComparable<SearchState>
        {
            public Field Field { get; private set; }
            public Field PrevField { get; private set; }
            public double Rating { get; private set; }
            public double AddedRating { get; private set; }
            public PlayerState PlayerState { get; private set; }
            public void AddRating(double rating)
            {
                Rating += rating;
            }
            public SearchState(Field prevField, Field field, PlayerState playerState, double rating, double addedRating)
            {
                PrevField = prevField;
                Field = field;
                PlayerState = playerState;
                Rating = rating;
                AddedRating = addedRating;
            }

            public int CompareTo(SearchState other)
            {
                return Rating.CompareTo(other.Rating);
            }
        }
        /// <summary>
        /// Gets moves for current round - search for the current and the next piece
        /// </summary>
        /// <param name="milliseconds">How many milliseconds do we have for this round</param>
        /// <returns>Set of moves (plan) for current round</returns>
        public Plan GetMoves(int milliseconds)
        {
            //validSearch = 0.0;
            BestPlan = new Plan();
            SearchState initialState = new SearchState(null, MyField, MyState, 0, 0);
            Search(initialState, 0);
            //Console.WriteLine($"Validation: {validSearch}");
            //Console.ReadLine();
            return BestPlan;
        }

        /// <summary>
        /// GetMoves equivalent for on-line play
        /// </summary>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public Plan MovesForRound(int milliseconds)
        {
            MyField.Piece = new Piece(GameState.CurrentPiece, MyField, Engine.GetPiecePosition(GameState.CurrentPiece));

#if ONLINE_LOG
            Console.WriteLine();
            Console.WriteLine(MyField);
            Console.WriteLine();
#endif

            return GetMoves(milliseconds);
        }

#if PLAY_SP
        public static List<int> validStates = new List<int>();
#endif

        #region Search
        private double Search(SearchState state, int depth)
        {
            //Stopwatch generationSw = new Stopwatch();
            //Stopwatch validationSw = new Stopwatch();
            //generationSw.Restart();
            IEnumerable<PieceState> pieceStates = state.Field.GenerateAllGroundedStates();
#if PLAY_SP
            validStates.Add(pieceStates.Count());
#endif
            //generationSw.Stop();
            IEnumerable<SearchState> searchStates = GenerateValidFields(pieceStates, state, GameState.Round + depth + 1);

            double bestRating = double.MinValue;
            SearchState bestState = null;
            foreach (var searchState in searchStates)
            {
                searchState.AddRating(state.AddedRating);

                double rating;
                if (depth == 1)
                    rating = searchState.Rating;
                else
                    rating = Search(searchState, depth + 1);

                if (rating > bestRating)
                {
                    bestRating = rating;
                    bestState = searchState;
                    }
                }

            if (depth == 0 && bestState != null)
                BestPlan = GetPlanForField(bestState.PrevField);

            return bestRating;
            }

        //Stopwatch validSearchSw = new Stopwatch();
        //double validSearch = 0.0;

        private IEnumerable<SearchState> GenerateValidFields(IEnumerable<PieceState> pieceStates, SearchState state, int round)
            {
            List<SearchState> states = new List<SearchState>();
            foreach (var ps in pieceStates)
                {
                Field field = GetFieldFor(ps, state.Field);

                Field newField;
                PlayerState newPlayerState = state.PlayerState.ShallowClone();
                FieldScore score = Engine.GetScore(newPlayerState, field, out newField);

                if (!score.IsLosing && Engine.PrepareForNextRound(newField, round, GameState.NextPiece, 0))
                {
                    // Is the state reachable?
                    //validSearchSw.Restart();
                    bool hasPlan = FastPlanSearch.HasPlan(field);
                    //validSearchSw.Stop();
                    //Console.WriteLine($"Has plan took: {validSearchSw.Elapsed.TotalMilliseconds}");
                    //validSearch += validSearchSw.Elapsed.TotalMilliseconds;
                    if (hasPlan)
                    {
                        double rating = RateField(newField, score);
                        double addedRating = RateScore(score.Score);
                        states.Add(new SearchState(field, newField, newPlayerState, rating, addedRating));
                }
            }
        }

            return states;
        }
#region FastPlanSearch
        
#endregion
        private Field GetFieldFor(PieceState state, Field field)
            {
            Field newField = field.Clone();
            newField.Piece.SetPieceState(state);
            return newField;
        }
        /// <summary>
        /// A* search for plan.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private Plan GetPlanForField(Field field) // TODO add turnLeft, turnRight if T spin is detected
                {
            PieceState originalState = field.GetPieceState();

            // Generate a state we can certainly get to
            field.SetPieceState(new PieceState(Engine.GetPiecePosition(field.Piece.Type), 0));
            Plan plan = new Plan();
            int toMoveDown = field.PrepareForSearch();
            for (int i = 0; i < toMoveDown; i++)
            {
                plan.Moves.AddLast(Move.Down);
                    }

            PieceState goal = field.GetPieceState();

            PlanSearch planSearch = new PlanSearch(originalState, goal, field);
            Plan found = planSearch.GetPlan();
            field.SetPieceState(originalState);

            if (found == null)
            {
                return new Plan();
                }
            else
            {
                // Is this worth it? Should not we use some other structure instead?
                foreach (var move in found.Moves)
                {
                    plan.Moves.AddLast(move);
                }
                return plan;
            }
        }
#endregion

        void IBot.UpdateGameState(GameState state)
        {
            if (GameState == null)
            {
                GameState = new GameState();
            }
            GameState.Round = state.Round;
            GameState.CurrentPiece = state.CurrentPiece;
            GameState.NextPiece = state.NextPiece;
            GameState.PiecePosition = state.PiecePosition;
        }

        void IBot.UpdatePlayerState(string name, PlayerState state)
        {
            if (!Players.ContainsKey(name))
            {
                PlayerState newState = new PlayerState();
                newState.Field = state.Field.Clone();

                Players.Add(name, newState);
            }
            else
            {
                Players[name].Combo = state.Combo;
                Players[name].RowPoints = state.RowPoints;
                Players[name].Field = state.Field.Clone();
            }
        }
    }
}
