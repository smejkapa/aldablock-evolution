﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AldaBlockEvo.GameNS;
// TODO: Uncomment following if using c# 6 and modify the code accordingly
//using static AldaBlockEvo.GameNS.Criteria;

namespace AldaBlockEvo.GameNS
{
    /*
    Criteria:
        0. PileHeight - The row of the highest occupied cell in the board
        1. Holes - Number of occupied that has at least one unoccupied above
        2. ConnectedHoles - Same as Holes above, however vertically connected unoccupied cells only count as one hole.
        3. RemovedLines - Number of removed lines
        4. AltitudeDifference - The difference between the highest occupied and the lowest free = directly reachable from the top
        5. MaximumWellDepth - The depth of the deepest well (width of 1)
        6. SumOfAllWells - Sum of depths of all wells
        7. LandingHeight - The height at which the last piece has been placed
        8. Blocks - Number of occupied cells on the board
        9. WeightedBlocks - Same as blocks but block at height n counts n times as block at height 1
        10. RowTransitions - Sum of all unoccupied/occupied transitions in a row
        11. ColumnTransitions - Same as row but for column
        12. ScoreGained - Total score gained this round
    */

    public enum CriteriaNames : byte
    {
        PileHeight = 0, // The row of the highest occupied cell in the board
        Holes, // Number of occupied that has at least one unoccupied above
        ConnectedHoles, // Same as Holes above, however vertically connected unoccupied cells only count as one hole.
        RemovedLines, // Number of removed lines
        AltitudeDifference, // The difference between the highest occupied and the lowest free = directly reachable from the top
        MaximumWellDepth, // The depth of the deepest well (width of 1)
        SumOfAllWells, // Sum of depths of all wells
        LandingHeight, // The height at which the last piece has been placed
        Blocks, // Number of occupied cells on the board
        WeightedBlocks, // Same as blocks but block at height n counts n times as block at height 1
        RowTransitions, // Sum of all unoccupied/occupied transitions in a row
        ColumnTransitions, // Same as row but for column
        ScoreGained, // Total score gained this round
        CriteriaCount
    }

    class FieldAnalysis
    {
        public int[] Criteria;

        public FieldAnalysis()
        {
            Criteria = new int[(int)CriteriaNames.CriteriaCount];
        }

        public override bool Equals(object other)
        {
            var toCompareWith = other as FieldAnalysis;
            if (toCompareWith == null)
                return false;
            if (Criteria.Length != toCompareWith.Criteria.Length)
                return false;
            for (int i = 0; i < Criteria.Length; i++)
            {
                if (Criteria[i] != toCompareWith.Criteria[i])
                    return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            int hash = 17;
            unchecked
            {
                for (int i = 0; i < Criteria.Length; i++)
                {
                    hash = hash * 23 + Criteria[i].GetHashCode();
                }
            }
            return hash;
        }
    }

    static class FieldAnalyzer
    {
        public static FieldAnalysis AnalyzeField(Field field, FieldScore score)
        {
            FieldAnalysis analysis = new FieldAnalysis();

            // Some things are better done line by line, other are better column by column
            HorizontalAnalysis(field, analysis);
            VerticalAnalysis(field, analysis);

            // Removed lines
            analysis.Criteria[(byte)CriteriaNames.RemovedLines] = score.LinesRemoved;

            // Height of the last piece
            analysis.Criteria[(byte)CriteriaNames.LandingHeight] = score.LastPieceHeight;

            // Score itself
            analysis.Criteria[(byte)CriteriaNames.ScoreGained] = score.Score;

            return analysis;
        }
        /// <summary>
        /// Line by line left to right analysis
        /// Analyses 0, 8, 9, 10
        /// </summary>
        /// <param name="field">Field to analyze</param>
        /// <param name="fa">Field analysis to write to</param>
        private static void HorizontalAnalysis(Field field, FieldAnalysis fa)
        {
            // Line by line analysis
            for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
            {
                int currentHeight = GlobalSettings.FIELD_HEIGHT - y;
                // Early escape - first solid block = end of the field
                if (field.Grid[0, y].Type == FieldCellType.Solid)
                {
                    // Highest pile
                    if (currentHeight > fa.Criteria[(byte)CriteriaNames.PileHeight])
                    {
                        fa.Criteria[(byte)CriteriaNames.PileHeight] = currentHeight;
                    }
                    break;
                }

                // All lines outside of the box are full
                bool full = true;
                
                for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
                {
                    if (field.Grid[x, y].Type == FieldCellType.Block)
                    {
                        // Sum of all blocks
                        ++fa.Criteria[(byte)CriteriaNames.Blocks];

                        // Weighted sum of blocks
                        fa.Criteria[(byte)CriteriaNames.WeightedBlocks] += currentHeight;

                        // Highest pile
                        if (currentHeight > fa.Criteria[(byte)CriteriaNames.PileHeight])
                        {
                            fa.Criteria[(byte)CriteriaNames.PileHeight] = currentHeight;
                        }

                        // Horizontal transitions
                        full = true;
                    }
                    else if (field.Grid[x, y].Type == FieldCellType.Empty)
                    {
                        // Horizontal transitions Block -> Empty
                        if (full)
                        {
                            ++fa.Criteria[(byte)CriteriaNames.RowTransitions];
                            full = false;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Column by column top to down analysis
        /// Analyses 1, 2, 4, 5, 6, 11
        /// </summary>
        /// <param name="field"></param>
        /// <param name="fa"></param>
        private static void VerticalAnalysis(Field field, FieldAnalysis fa)
        {
            int altitudeMin = GlobalSettings.FIELD_HEIGHT;
            int deepestWell = 0;
            // Column by column analysis
            for (int x = 0; x < GlobalSettings.FIELD_WIDTH; x++)
            {
                bool full = true;
                bool firstBlock = false;
                bool newBlock = false;
                bool wellStarted = false;
                int currentWellDepth = 0;

                for (int y = 0; y < GlobalSettings.FIELD_HEIGHT; y++)
                {
                    // Early escape - first solid block = end of current column
                    if (field.Grid[x, y].Type == FieldCellType.Solid)
                        break;

                    int currentHeight = GlobalSettings.FIELD_HEIGHT - y;
                    if (field.Grid[x, y].Type == FieldCellType.Block)
                    {
                        // Every empty cell under first block is a hole
                        firstBlock = true;
                        // Only new blocks create vertically connected holes
                        newBlock = true;

                        // Vertical transitions
                        full = true;
                    }
                    else if (field.Grid[x, y].Type == FieldCellType.Empty)
                    {
                        // Vertical transitions Block -> Empty
                        if (full)
                        {
                            ++fa.Criteria[(byte)CriteriaNames.ColumnTransitions];
                            full = false;
                        }
                        // Holes
                        if (firstBlock)
                        {
                            ++fa.Criteria[(byte)CriteriaNames.Holes];
                        }
                        // Verically connected holes
                        if (newBlock)
                        {
                            ++fa.Criteria[(byte)CriteriaNames.ConnectedHoles];
                            newBlock = false;
                        }
                        // Altitude difference
                        if (!firstBlock && altitudeMin > currentHeight)
                        {
                            altitudeMin = currentHeight;
                        }
                        // Wells - is there a hole of width one
                        if (!firstBlock && !wellStarted
                            && (x == 0 || field.Grid[x - 1, y].Type == FieldCellType.Block)
                            && (x == GlobalSettings.FIELD_WIDTH - 1 || field.Grid[x + 1, y].Type == FieldCellType.Block))
                        {
                            wellStarted = true;
                        }
                        if (wellStarted && !firstBlock)
                        {
                            ++fa.Criteria[(byte)CriteriaNames.SumOfAllWells];
                            ++currentWellDepth;
                        }
                    }
                }
                if (currentWellDepth > deepestWell)
                {
                    deepestWell = currentWellDepth;
                }
            }
            // Altitude difference - if the board is perfectly cleared altDiff = -1
            fa.Criteria[(byte)CriteriaNames.AltitudeDifference] = fa.Criteria[(byte)CriteriaNames.PileHeight] - altitudeMin;
            // Deepest well
            fa.Criteria[(byte)CriteriaNames.MaximumWellDepth] = deepestWell;
        }
    }
}
