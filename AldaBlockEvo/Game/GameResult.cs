﻿using System.Collections.Generic;

namespace AldaBlockEvo.GameNS
{
    enum Winner
    {
        Draw = -1,
        First = 0,
        Second = 1
    }

    class GameResult
    {
        public List<int> Score { get; private set; }
        public int RoundCount { get; private set; }
        public Winner Winner { get; private set; }

        public GameResult(Winner winner, int roundCount, List<int> score)
        {
            Score = score;
            RoundCount = roundCount;
            Winner = winner;
        }
    }
}
