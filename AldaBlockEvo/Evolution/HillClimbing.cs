﻿#define VERBOSE_HC
//#define PLAY_ONLINE

using AldaBlockEvo.GameNS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

#if !PLAY_ONLINE
using static AldaBlockEvo.GlobalSettings;
#endif

namespace AldaBlockEvo.EvolutionNS
{
    class HillClimbing : IDisposable
    {
#if !PLAY_ONLINE
        private readonly int POPULATION_SIZE;
        private double MutationProba;
        private readonly double GAUSS_MEAN;
        private double GausStdDev;
        private readonly double GAUSS_STD_DEV_MIN;
        private readonly double GAUSS_STD_DEV_MAX;
        private readonly double MUTATION_PROBA_MIN;
        private readonly double MUTATION_PROBA_MAX;
        private readonly int N_BEST_TO_TAKE;

        /// <summary>
        ///  Contains current population
        /// </summary>
        private RatingWeights[] Weights;
        private readonly IBot[] Bots;

        private class Individual
        {
            public RatingWeights Weights { get; private set; }
            public Individual(RatingWeights weights)
            {
                Weights = weights;
            }
        }
        private List<Individual> BestIndividuals;
        private StreamWriter BestIndividualsSw;
        private StreamWriter GenLogSw;

        public HillClimbing()
        {
            GAUSS_MEAN = 1.0;
            GAUSS_STD_DEV_MIN = 0.25;
            GAUSS_STD_DEV_MAX = 0.50;
            GausStdDev = 0.45;

            POPULATION_SIZE = 128;
            MUTATION_PROBA_MIN = 0.3;
            MUTATION_PROBA_MAX = 1.0;
            MutationProba = 0.05;

            N_BEST_TO_TAKE = 4; // Keep at 4 for now - New population generation will work only with 4 ATM

            Weights = new RatingWeights[POPULATION_SIZE];
            Bots = new IBot[POPULATION_SIZE];
            BestIndividuals = new List<Individual>();

            if (!Directory.Exists(HC_LOG_DIR))
                Directory.CreateDirectory(HC_LOG_DIR);

            BestIndividualsSw = new StreamWriter($@"{HC_LOG_DIR}[{DATE_TIME_FILE_FORMAT}]_bestIndividuals.txt");
            GenLogSw = new StreamWriter($@"{HC_LOG_DIR}[{DATE_TIME_FILE_FORMAT}]_generations.txt");
        }

        void ValidateSettings()
        {
            // The population needs to be a power of 2 (for simplicity of the binary tournament etc.)
            if (POPULATION_SIZE <= 0 || !Helpers.IsPow2(POPULATION_SIZE))
            {
                throw new InvalidPopulationSizeException($"The population size needs to be > 0 and a power of two. Current size: {POPULATION_SIZE}");
            }

            if (POPULATION_SIZE != 128)
            {
                throw new InvalidPopulationSizeException($"The population size needs to be 128 right now for NewGenFrom() to work. Current size: {POPULATION_SIZE}");
            }
        }
        private class SavedState
        {
            public int Generation { get; set; }
            public List<RatingWeights> Weights { get; }
            public SavedState()
            {
                Weights = new List<RatingWeights>();
            }
        }

        private SavedState ParseSavedState()
        {
            const string savedStatePath = HC_SETTINGS_DIR + "savedState.txt";
            if (!File.Exists(savedStatePath))
            {
                throw new FileNotFoundException(savedStatePath + " file does not exist");
            }

            using (StreamReader savedSr = new StreamReader(savedStatePath))
            {
                SavedState state = new SavedState();
                state.Generation = int.Parse(savedSr.ReadLine());
                for (int i = 0; i < N_BEST_TO_TAKE; i++)
                {
                    state.Weights.Add(new RatingWeights(savedSr.ReadLine()));
                }
                return state;
            }
        }
        public void Run(bool continuteFromFile = false)
        {
            ValidateSettings();
#if VERBOSE_HC
            Console.WriteLine("Hill climbing started!");
#endif
            int generation;
            if (continuteFromFile)
            {
                var state = ParseSavedState();
                generation = state.Generation + 1;
                Weights = NewGenFrom(state.Weights);
                AssignWeights();
            }
            else // Start new hill climbing with random population
            {
                InitPopulation();
                generation = 0;
            }

            Stopwatch tournamentSw = new Stopwatch();
            while (true)
            {
#if VERBOSE_HC
                Console.WriteLine($"Tournament {generation} has started. {DateTime.Now.ToShortTimeString()}");
                tournamentSw.Restart();
#endif
                LogGeneration(generation);
                BinaryTournament tournament = new BinaryTournament(Bots);
                var result = tournament.Run(N_BEST_TO_TAKE);

#if VERBOSE_HC
                tournamentSw.Stop();
                Console.WriteLine($"Tournament finished in {tournamentSw.Elapsed.ToString()}");
#endif
                for (int j = 0; j < N_BEST_TO_TAKE; j++)
                {
                    BestIndividuals.Add(new Individual(result.Order[j].GetWeights));
                }

                Weights = NewGenFrom(result.Order.AsWeights());
                AssignWeights();

                LogProgress(generation);
                LogBestIndividual(generation);
                ++generation;
            }
        }
        /// <summary>
        /// Assigns current population of weights - creates new bots
        /// </summary>
        private void AssignWeights()
        {
            for (int i = 0; i < POPULATION_SIZE; i++)
            {
                Bots[i] = new Bot(Weights[i]);
            }
        }

        #region Logging
        private void LogBestIndividual(int generation)
        {
            BestIndividualsSw.WriteLine($"--- Generation {generation}");
            // Log in order best to worst
            for (int i = N_BEST_TO_TAKE; i > 0; i--)
            {
                Individual best = BestIndividuals[BestIndividuals.Count - i];
                BestIndividualsSw.WriteLine($"\t{N_BEST_TO_TAKE - i + 1}: {best.Weights}"); // Position of the bot at the beginning
            }
            BestIndividualsSw.Flush();
        }
        private void LogProgress(int generation)
        {
            Console.WriteLine();
            Console.WriteLine("-----------------------------");
            Console.WriteLine($"Generation {generation} has finished.");
            Console.WriteLine("-----------------------------");
            Console.WriteLine();
        }
        private void LogGeneration(int generation)
        {
            GenLogSw.WriteLine($"--- Generation {generation}");
            for (int i = 0; i < Weights.Length; i++)
            {
                GenLogSw.WriteLine($"\t{Weights[i]}");
            }
            GenLogSw.Flush();
        }
        #endregion
        /// <summary>
        /// Initializes first population with random values
        /// </summary>
        private void InitPopulation()
        {
            for (int botIdx = 0; botIdx < POPULATION_SIZE; botIdx++)
            {
                Weights[botIdx] = new RatingWeights();
                for (int ratingIdx = 0; ratingIdx < (int)CriteriaNames.CriteriaCount; ratingIdx++)
                {
                    if (IGNORE_CRIT_SET.Contains((CriteriaNames)ratingIdx))
                    {
                        Weights[botIdx].Ratings[ratingIdx] = new WeightTuple(0, 0);
                    }
                    else
                    {
                        // Add 10 to avoid very low numbers right from the start
                        Weights[botIdx].Ratings[ratingIdx] = new WeightTuple((Helpers.TSRandom.Next(100) + 10) * GlobalSettings.RATING_SIGN[ratingIdx], (Helpers.TSRandom.Next(100) + 50) / 100.0);
                    }
                }
                Bots[botIdx] = new Bot(Weights[botIdx]);
            }
        }

        /// <summary>
        /// Currently works only if bots.Count == 4
        /// </summary>
        /// <param name="bots"></param>
        /// <returns></returns>
        private RatingWeights[] NewGenFrom(List<RatingWeights> weights)
        {
            List<RatingWeights> newWeights = new List<RatingWeights>();
            int shift = 1;
            for (int i = 0; i < weights.Count; i++)
            {
                // The population should be proportional size for each bot is proportional to the position it achieved in the tournament
                if (Helpers.IsPow2(i))
                {
                    ++shift;
                }

                int currentBotPopulation = (int)(POPULATION_SIZE * (1.0 / (1 << shift)));
                for (int count = 0; count < currentBotPopulation; ++count)
                {
                    // Copy the original to avoid premature convergence
                    if (count <  4)
                    {
                        MutationProba = 0;
                    }
                    else // Create mutations of the original
                    {
                        MutationProba = Helpers.Lerp(MUTATION_PROBA_MIN, MUTATION_PROBA_MAX, count, currentBotPopulation - 1);
                        GausStdDev = Helpers.Lerp(GAUSS_STD_DEV_MIN, GAUSS_STD_DEV_MAX, count, currentBotPopulation - 1);
                    }
                    newWeights.Add(CreateMutation(weights[i]));
                }
            }

            if (newWeights.Count != POPULATION_SIZE)
            {
                throw new InvalidPopulationSizeException($"New population must have the same size as the previous one. New: {newWeights.Count}, Previous: {POPULATION_SIZE}");
            }

            return newWeights.ToArray();
        }

        
        private RatingWeights CreateMutation(RatingWeights individual)
        {
            var newIndividual = individual.DeepClone();
            Mutation(newIndividual);
            return newIndividual;
        }
        private void Mutation(RatingWeights individual)
        {
            // Try to mutate each genome - each weight and exponent may be mutated
            for (int i = 0; i < individual.Ratings.Length; i++)
            {
                // Ignore certain specified criteria
                if (IGNORE_CRIT_SET.Contains((CriteriaNames)i))
                    continue;

                if (Helpers.TSRandom.NextDouble() > MutationProba)
                {
                    // Restart the gene in case of if being off
                    if (individual.Ratings[i].Sigma == 0)
                    {
                        individual.Ratings[i].Sigma = RATING_SIGN[i] * 10;
                    }
                    else
                    {
                        individual.Ratings[i].Sigma = (int)(individual.Ratings[i].Sigma * Helpers.GetGaussRand(GAUSS_MEAN, GausStdDev));
                    }
                }
                if (Helpers.TSRandom.NextDouble() > MutationProba)
                {
                    // Restart the gene in case of if being off
                    if (individual.Ratings[i].Exponent == 0.0)
                    {
                        individual.Ratings[i].Exponent = 0.001;
                    }
                    else
                    {
                        individual.Ratings[i].Exponent = individual.Ratings[i].Exponent * Helpers.GetGaussRand(GAUSS_MEAN, GausStdDev);
                    }
                }
            }
        }
#endif
        public void Dispose()
        {
#if !PLAY_ONLINE
            BestIndividualsSw.Close();
            GenLogSw.Close();

            BestIndividualsSw.Dispose();
            GenLogSw.Dispose();
#endif
        }
    }
}

