﻿#define VERBOSE_EVO
//#define PLAY_ONLINE

using AldaBlockEvo.GameNS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

#if !PLAY_ONLINE
using static AldaBlockEvo.GlobalSettings;
#endif

namespace AldaBlockEvo.EvolutionNS
{
    class Evolution : IDisposable
    {
#if !PLAY_ONLINE
        private readonly int POPULATION_SIZE;
        private readonly int ELITE_SIZE;
        private double MutationProba;
        private readonly uint GENERATION_THOLD;
        private readonly uint TOURNAMENT_SIZE;
        private readonly uint GAME_COUNT;
        private readonly double GausMean;
        private double GausStdDev;
        private readonly double GAUSS_STD_DEV_MIN;
        private readonly double MUTATION_PROBA_MIN;
        private readonly double STD_DEV_DIFF;
        private readonly double MUTATION_DIFF;

        private readonly int[] Fitness;
        /// <summary>
        ///  Contains current population
        /// </summary>
        private RatingWeights[] Weights;
        /// <summary>
        ///  Do we want the evolution to handle bots or just weights???
        /// </summary>
        private readonly IBot[] Bots;

        private readonly int[] GameSeeds;

        private static readonly Random rnd = new Random();

        private class Individual
        {
            public RatingWeights Weights { get; private set; }
            public int Fitness { get; private set; }
            public Individual (RatingWeights weights, int fitness)
            {
                Weights = weights;
                Fitness = fitness;
            }
        }
        private List<Individual> BestIndividuals;
        private StreamWriter BestIndividualsSw;
        private StreamWriter GenLogSw;
        private StreamWriter SelectionSw;
        private StreamWriter TournamentLogSw;

        private class SelectionClass
        {
            public List<Individual> Parents;
            public Individual Child;
            public Individual MutatedChild;
            public SelectionClass()
            {
                Parents = new List<Individual>();
            }
        }
        private SelectionClass[] Selections;

        private class Tournament
        {
            public List<Individual> Contestants;
            public Individual Winner;
            public Tournament()
            {
                Contestants = new List<Individual>();
            }
        }
        private Tournament[] Tournaments;

        public Evolution()
        {
            GausMean = 1.0;
            GausStdDev = 0.45;
            GAUSS_STD_DEV_MIN = 0.35;
            STD_DEV_DIFF = 0.01;

            POPULATION_SIZE = 50;
            ELITE_SIZE = POPULATION_SIZE / 10;
            MutationProba = 0.05;
            MUTATION_PROBA_MIN = 0.015;
            MUTATION_DIFF = 0.001;
            GENERATION_THOLD = 500;
            TOURNAMENT_SIZE = 2;
            GAME_COUNT = 10;

            Fitness = new int[POPULATION_SIZE];
            Weights = new RatingWeights[POPULATION_SIZE];
            Bots = new IBot[POPULATION_SIZE];
            BestIndividuals = new List<Individual>();
            Selections = new SelectionClass[POPULATION_SIZE];
            Tournaments = new Tournament[POPULATION_SIZE * 2];
            GameSeeds = new int[GAME_COUNT];

            if (!Directory.Exists(EVO_LOG_DIR))
                Directory.CreateDirectory(EVO_LOG_DIR);

            BestIndividualsSw = new StreamWriter($@"{EVO_LOG_DIR}[{DATE_TIME_FILE_FORMAT}]_bestIndividuals.txt");
            GenLogSw = new StreamWriter($@"{EVO_LOG_DIR}[{DATE_TIME_FILE_FORMAT}]_generations.txt");
            SelectionSw = new StreamWriter($@"{EVO_LOG_DIR}[{DATE_TIME_FILE_FORMAT}]_selections.txt");
            TournamentLogSw = new StreamWriter($@"{EVO_LOG_DIR}[{DATE_TIME_FILE_FORMAT}]_tournaments.txt");
        }

        public void Run()
        {
#if VERBOSE_EVO
            Console.WriteLine("Evolution started!");
#endif
            InitPopulation();
#if VERBOSE_EVO
            Console.WriteLine("First population initialized");
#endif
            for (int i = 0; i < GENERATION_THOLD; i++)
            {
                GenerateGameSeeds();
#if VERBOSE_EVO
            Console.WriteLine("Calculating fitness started");
#endif
                CalculateFitness();
#if VERBOSE_EVO
            Console.WriteLine("Fitness calculated");
#endif
                BestIndividuals.Add(FindBestIndividual());

                LogGeneration(i);

                Weights = NextGeneration();
                AssignWeights();

                LogProgress(i);
                LogBestIndividual();

                ChangeProbas();
            }

            BestIndividualsSw.Close();
        }
        private void ChangeProbas()
        {
            if (GausStdDev > GAUSS_STD_DEV_MIN)
            {
                GausStdDev -= STD_DEV_DIFF;
            }
            if (MutationProba > MUTATION_PROBA_MIN)
            {
                MutationProba -= MUTATION_DIFF;
            }
        }
        private void GenerateGameSeeds()
        {
            for (int i = 0; i < GAME_COUNT; i++)
            {
                GameSeeds[i] = rnd.Next();
            }
        }
        /// <summary>
        /// Assigns current population of weights - creates new bots
        /// </summary>
        private void AssignWeights()
        {
            for (int i = 0; i < POPULATION_SIZE; i++)
            {
                Bots[i] = new Bot(Weights[i]);
            }
        }
        private Individual FindBestIndividual()
        {
            int? maxVal = null;
            int index = -1;
            for (int i = 0; i < Fitness.Length; i++)
            {
                int current = Fitness[i];
                if (!maxVal.HasValue || current > maxVal.Value)
                {
                    maxVal = current;
                    index = i;
                }
            }
            return new Individual(Weights[index], Fitness[index]);
        }
        private void LogBestIndividual()
        {
            Individual best = BestIndividuals[BestIndividuals.Count - 1];
            BestIndividualsSw.WriteLine($"{best.Fitness}: {best.Weights}");
            BestIndividualsSw.Flush();
        }
        private void LogProgress(int generation)
        {
            int[] myFitness = (int[])Fitness.Clone();
            Array.Sort(myFitness);
            int medianFitness = myFitness[(int)Math.Floor(myFitness.Length / 2.0)];
            int secondHighest = myFitness[myFitness.Length - 2];
            float avgFitness = myFitness.Sum() / myFitness.Length;

            Console.WriteLine();
            Console.WriteLine("-----------------------------");
            Console.WriteLine($"Generation {generation} has finished.");
            Console.WriteLine($"Best individual's fitness: {BestIndividuals[BestIndividuals.Count - 1].Fitness}");
            Console.WriteLine($"Second Best: {secondHighest}, Median: {medianFitness}, Avg: {avgFitness}");
            Console.WriteLine("-----------------------------");
            Console.WriteLine();

            LogSelection(generation);
        }
        private void LogGeneration(int generation)
        {
            var sorted = Fitness
                .Select((x, i) => new KeyValuePair<int, int>(x, i))
                .OrderByDescending(x => x.Key)
                .ToList();

            var weightIdcs = sorted.Select(x => x.Value).ToList();
            GenLogSw.WriteLine($"--- Generation {generation}");
            for (int i = 0; i < weightIdcs.Count; i++)
            {
                int idx = weightIdcs[i];
                GenLogSw.WriteLine($"\t{Fitness[idx]} ({idx}): {Weights[idx]}");
            }
            GenLogSw.Flush();
        }
        private void LogSelection(int generation)
        { 
            // Log tournaments
            TournamentLogSw.WriteLine($"------------------------------");
            TournamentLogSw.WriteLine($"Generation {generation}");
            TournamentLogSw.WriteLine($"------------------------------");
            for (int i = 0; i < Tournaments.Length; i++)
            {
                TournamentLogSw.WriteLine($"Tournament {i}");
                if (Tournaments[i] != null)
                {
                    foreach (var c in Tournaments[i].Contestants)
                    {
                        TournamentLogSw.WriteLine($"\t{c.Fitness}: {c.Weights}");
                    }
                    TournamentLogSw.WriteLine($"\tWinner: {Tournaments[i].Winner.Fitness}: {Tournaments[i].Winner.Weights}");
                }
                else
                {
                    TournamentLogSw.WriteLine($"\tElite");
                }
            }
            TournamentLogSw.Flush();

            // Log selections
            SelectionSw.WriteLine($"------------------------------");
            SelectionSw.WriteLine($"Generation {generation}");
            SelectionSw.WriteLine($"------------------------------");
            for (int i = 0; i < Selections.Length; i++)
            {
                SelectionSw.WriteLine($"Selection {i}");
                if (Selections[i] != null)
                {
                    foreach (var p in Selections[i].Parents)
                    {
                        SelectionSw.WriteLine($"\tParent {p.Fitness}: {p.Weights}");
                    }
                    SelectionSw.WriteLine($"\tChild {Selections[i].Child.Fitness}: {Selections[i].Child.Weights}");
                    SelectionSw.WriteLine($"\tMutated child {Selections[i].MutatedChild.Fitness}: {Selections[i].MutatedChild.Weights}");
                }
                else
                {
                    SelectionSw.WriteLine($"\tElite");
                }
            }
            SelectionSw.Flush();
        }
        /// <summary>
        /// Initializes first population with random values
        /// </summary>
        private void InitPopulation()
        {
            for (int i = 0; i < POPULATION_SIZE; i++)
            {
                Weights[i] = new RatingWeights();
                for (int j = 0; j < (int)CriteriaNames.CriteriaCount; j++)
                {
                    // Add 10 to avoid very low numbers right from the start
                    Weights[i].Ratings[j] = new WeightTuple((rnd.Next(100) + 10) * GlobalSettings.RATING_SIGN[j], (rnd.Next(100) + 50) / 100.0);
                }
                Bots[i] = new Bot(Weights[i]);
            }
        }
        /// <summary>
        /// Starts a single game instance with a given bot
        /// </summary>
        /// <param name="bot">The bot that plays the game</param>
        /// <returns>A performance of given bot</returns>
        private int PlayGame(IBot bot)
        {
            IGame engine = new Game(new IBot[] { bot });

            GameResult worstResult = null;
            for (int i = 0; i < GAME_COUNT; i++)
            {
                var result = engine.StartGame(GameSeeds[i]);
                if (worstResult == null || worstResult.Score[0] > result.Score[0])
                    worstResult = result;
            }

            if (worstResult == null)
                throw new ArgumentOutOfRangeException("There has to be at least one game played");

            // The worst performance
            return worstResult.Score[0];
        }
        private void CalculateFitness()
        {
            // Start all games in parallel and assign the calculated fitness
            Task[] games = new Task[Bots.Length];
            for (int i = 0; i < Bots.Length; i++)
            {
                int idx = i;
                games[i] = Task.Run(() => Fitness[idx] = PlayGame(Bots[idx]));
            }

            Task.WaitAll(games);
        }

        private List<RatingWeights> SelectElite()
        {
            List<RatingWeights> elite = new List<RatingWeights>();
            var sorted = Fitness
                .Select((x, i) => new KeyValuePair<int, int>(x, i))
                .OrderByDescending(x => x.Key)
                .ToList();

            List<int> weightIdcs = sorted.Select(x => x.Value).Take(ELITE_SIZE).ToList();
            for (int i = 0; i < weightIdcs.Count; i++)
            {
                elite.Add(Weights[weightIdcs[i]]);
            }

            return elite;
        }
        private RatingWeights[] NextGeneration()
        {
            RatingWeights[] nextGeneration = new RatingWeights[POPULATION_SIZE];
            
            // Elite selection so we do not lose the best individuals
            List<RatingWeights> elite = SelectElite();
            for (int i = 0; i < ELITE_SIZE; i++)
            {
                nextGeneration[i] = elite[i];
            }

            List<Task> newIndividuals = new List<Task>();
            for (int i = ELITE_SIZE; i < POPULATION_SIZE; i++)
            {
                int idx = i;
                newIndividuals.Add(Task.Run(() => nextGeneration[idx] = NewIndividual(idx)));
            }

            Task.WaitAll(newIndividuals.ToArray());

            return nextGeneration;
        }
        /// <summary>
        /// Performs selection, crossover and mutation to create a new individual
        /// </summary>
        /// <returns>Mutated child created from two selected parents</returns>
        private RatingWeights NewIndividual(int idx)
        {
            Selections[idx] = new SelectionClass();
            // Create a child from two parents
            var p1 = Selection(idx, idx);
            var p2 = Selection(idx, idx + POPULATION_SIZE);
            RatingWeights child = Crossover(p1, p2);
            Selections[idx].Child = new Individual(child.DeepClone(), -1);
            Mutation(child);

            Selections[idx].MutatedChild = new Individual(child.DeepClone(), -1);

            return child;
        }
        private RatingWeights Selection(int selIdx, int tournIdx)
        {
            Tournament t = new Tournament();
            // Find [TournamentSize] of random individuals and get the best one of them
            int? bestFitness = null;
            int bestIdx = 0;
            for (int i = 0; i < TOURNAMENT_SIZE; i++)
            {
                int idx = Helpers.TSRandom.Next(POPULATION_SIZE);
                t.Contestants.Add(new Individual(Weights[idx], Fitness[idx]));
                if (bestFitness == null || Fitness[idx] > bestFitness)
                {
                    bestFitness = Fitness[idx];
                    bestIdx = idx;
                }
            }

            t.Winner = new Individual(Weights[bestIdx], bestFitness ?? -1);
            Tournaments[tournIdx] = t;
            Selections[selIdx].Parents.Add(new Individual(Weights[bestIdx], bestFitness ?? -1));

            return Weights[bestIdx];
        }

        /// <summary>
        /// Two point crossover
        /// </summary>
        /// <param name="w1">First parent</param>
        /// <param name="w2">Second parent</param>
        /// <returns>New individual created from the two parents</returns>
        private RatingWeights Crossover(RatingWeights w1, RatingWeights w2)
        {
            // Find the two random points
            // Length + 1 To be able to have the last weight from the second parent
            int p1 = Helpers.TSRandom.Next(w1.Ratings.Length + 1);
            int p2 = Helpers.TSRandom.Next(w1.Ratings.Length + 1);
            // p1 must be <= to p2 - order is important for the for loop
            if (p1 > p2)
            {
                var tmp = p1;
                p1 = p2;
                p2 = tmp;
            }

            RatingWeights child = w1.DeepClone();
            for (int i = p1; i < p2; i++)
            {
                child.Ratings[i] = w2.Ratings[i];
            }

            return child;
        }
        
        private void Mutation(RatingWeights individual)
        {
            // Try to mutate each genome - each weight and exponent may be mutated
            for (int i = 0; i < individual.Ratings.Length; i++)
            {
                if (Helpers.TSRandom.NextDouble() > MutationProba)
                {
                    // Restart the gene in case of if being off
                    if (individual.Ratings[i].Sigma == 0)
                    {
                        individual.Ratings[i].Sigma = RATING_SIGN[i] * 10;
                    }
                    else
                    {
                        individual.Ratings[i].Sigma = (int)(individual.Ratings[i].Sigma * Helpers.GetGaussRand(GausMean, GausStdDev));
                    }
                }
                if (Helpers.TSRandom.NextDouble() > MutationProba)
                {
                    // Restart the gene in case of if being off
                    if (individual.Ratings[i].Exponent == 0.0)
                    {
                        individual.Ratings[i].Exponent = 0.001;
                    }
                    else
                    {
                        individual.Ratings[i].Exponent = individual.Ratings[i].Exponent * Helpers.GetGaussRand(GausMean, GausStdDev);
                    }
                }
            }
        }
#endif
        public void Dispose()
        {
#if !PLAY_ONLINE
            BestIndividualsSw.Dispose();
            GenLogSw.Dispose();
            SelectionSw.Dispose();
            TournamentLogSw.Dispose();
#endif
        }
    }
}

