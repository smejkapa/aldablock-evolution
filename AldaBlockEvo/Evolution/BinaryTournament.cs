﻿//#define PLAY_ONLINE
using AldaBlockEvo.GameNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo.EvolutionNS
{
#if !PLAY_ONLINE
    class TournamentResult
    {
        /// <summary>
        /// Finishing order of the tournament index 0 = the winner.
        /// There is no defined ordering for each row in the tournament tree.
        /// Third place is not defined (3rd is either on index 2 or 3, since game has not been played between these two).
        /// </summary>
        public List<IBot> Order;
    }

    class TournamentSeries
    {
        // All series have the same format = Best of GAME_COUNT
        const int GAME_COUNT = 17;

        public TournamentSeries Left { get; set; }
        public TournamentSeries Right { get; set; }
        private IBot[] Bots;

        public bool HasFinished { get; private set; }
        public IBot Winner { get { return Bots == null || !HasFinished ? null : Bots[0]; } }
        public IBot Loser { get { return Bots == null || !HasFinished ? null : Bots[1]; } }

        public TournamentSeries(TournamentSeries left, TournamentSeries right)
        {
            Left = left;
            Right = right;
        }

        /// <summary>
        /// Constructor for leaf-games
        /// </summary>
        /// <param name="bots"></param>
        public TournamentSeries(IBot[] bots)
        {
            Bots = bots;
        }

        public IBot PlayGame()
        {
            // Do we have bots to play the match with?
            if (Bots == null)
            {
                Bots = new IBot[2];
                var leftGame = Task.Run(() => Left.PlayGame());
                var rightGame = Task.Run(() => Right.PlayGame());
                
                // Wait all so we do not wake up when only one game has finished
                Task.WaitAll(new Task[] { leftGame, rightGame });
                Bots[0] = leftGame.Result;
                Bots[1] = rightGame.Result;
            }

            // Play the actual game here
            IGame engine = new Game(Bots);

            int[] matches = new int[2];
            int[] scores = new int[2];
            for (int i = 0; i < GAME_COUNT; i++)
            {
                var result = engine.StartGame(Helpers.TSRandom.Next());

                scores[0] += result.Score[0];
                scores[1] += result.Score[1];

                if (result.Winner != GameNS.Winner.Draw)
                {
                    ++matches[(int)result.Winner];
                    // GAME_COUNT / 2 is how many games one needs to win the series
                    if (matches[0] > GAME_COUNT / 2 || matches[1] > GAME_COUNT / 2)
                        break;
                }
            }

            WinningOrder(matches, scores);
            HasFinished = true;

            return Winner;
        }

        private void WinningOrder(int[] matches, int[] scores)
        {
            // Determine and return the winner
            int winner;
            if (matches[0] > matches[1])
                winner = 0;
            else if (matches[1] > matches[0])
                winner = 1;
            else if (scores[0] > scores[1]) // Both won equal amount of matches
                winner = 0;
            else // If the score is also draw - let's say that number just one got lucky
                winner = 1;

            // Reorder only if needed - winner is at Bots[0]
            if (winner == 1)
            {
                IBot tmp = Bots[0];
                Bots[0] = Bots[1];
                Bots[1] = tmp;
            }
        }
    }

    class BinaryTournament
    {
        readonly private IBot[] Bots;
        readonly private Random Rnd;
        private Stack<IBot[]> Duels;

        public BinaryTournament(IBot[] players)
        {
            Bots = players;
            Rnd = new Random();
        }

        /// <summary>
        /// Gets duels and returns them one by one
        /// </summary>
        /// <param name="count">How many duels to generate (should be Bots.Length / 2)</param>
        /// <returns>Duels as IEnumerable - one by one</returns>
        private IEnumerable<IBot[]> GetDuels(int count, List<int> pot)
        {
            for (int i = 0; i < count; i++)
            {
                IBot[] duel = new IBot[2];
                for (int j = 0; j < duel.Length; j++)
                {
                    duel[j] = Bots[pot[i * 2 + j]];
                }
                yield return duel;
            }
        }
        private TournamentSeries BuildTree(int depth, int leafLevel)
        {
            TournamentSeries game;
            if (depth == leafLevel)
            {
                game = new TournamentSeries(Duels.Pop());
            }
            else
            {
                game = new TournamentSeries(BuildTree(depth + 1, leafLevel), BuildTree(depth + 1, leafLevel));
            }
            return game;
        }
        private TournamentSeries GenerateTournament()
        {
            // Generate the duels
            List<int> pot = new List<int>(Enumerable.Range(0, Bots.Length));
            pot.QuickShuffle();
            Duels = new Stack<IBot[]>();

            foreach (IBot[] duel in GetDuels(Bots.Length / 2, pot))
            {
                Duels.Push(duel);
            }

            // Depth = 1 because of how the log works (or use 0 and subtract 1 from the log)
            return BuildTree(1, (int)Math.Log(Bots.Length, 2)); // TODO play with bits here instead of log maybe?
        }

        public TournamentResult Run(int orderDepth)
        {
            // Generate and play the tournament
            TournamentSeries root = GenerateTournament();
            root.PlayGame();

            // Determine order and return the result
            var result = new TournamentResult();
            var determinator = new OrderDeterminator();
            result.Order = determinator.DetermineOrder(root, orderDepth);
            return result;
        }

        private class OrderDeterminator
        {
            Queue<TournamentSeries> Open;

            public List<IBot> DetermineOrder(TournamentSeries root, int count)
            {
                if (!root.HasFinished)
                    throw new ArgumentException("Root game has to finish before determining order.");

                List<IBot> order = new List<IBot>();
                order.Add(root.Winner);
                --count;

                // BFS until we have enough bots to return
                Open = new Queue<TournamentSeries>();
                Open.Enqueue(root);

                while (count > 0 && Open.Count > 0)
                {
                    var current = Open.Dequeue();
                    Expand(Open, current);

                    order.Add(current.Loser);
                    --count;
                }

                return order;
            }
            private void Expand(Queue<TournamentSeries> open, TournamentSeries game)
            {
                if (game.Left == null || game.Right == null)
                    return;

                open.Enqueue(game.Left);
                open.Enqueue(game.Right);
            }
        }
    }
#endif
}
