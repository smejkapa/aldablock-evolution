﻿using AldaBlockEvo.GameNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo
{
    static class Helpers
    {
        public static bool IsPow2(int x)
        {
            return (x != 0) && (x & (x - 1)) == 0;
        }

        private static readonly Random rnd = new Random();
        [ThreadStatic]
        private static Random _TSRandom;
        public static Random TSRandom
        {
            get
            {
                if (_TSRandom == null)
                {
                    int seed;
                    lock (rnd)
                    {
                        seed = rnd.Next();
                    }
                    _TSRandom = new Random(seed);
                }
                return _TSRandom;
            }
        }

        public static double Lerp(double x1, double x2, double current, double max)
        {
            return Lerp(x1, x2, current / max);
        }

        public static double Lerp(double x1, double x2, double amount)
        {
            return x1 + (x2 - x1) * amount;
        }

        /// <summary>
        /// Uses Box–Muller transform to generate a random number Gaussian distribution.
        /// </summary>
        /// <returns>Random number from Gaussian distribution</returns>
        public static double GetGaussRand(double mean, double stdDev)
        {
            double u1 = TSRandom.NextDouble();
            double u2 = TSRandom.NextDouble();
            // Random number from normal distribution
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
            // Modify to fit our needs
            return mean + stdDev * randStdNormal;
        }

        public static int ManhattanDistace(PieceState from, PieceState to, int rotationCount)
        {
            return (Math.Abs(from.Position.X - to.Position.X)
                + Math.Abs(from.Position.Y - to.Position.Y)
                + Math.Min(Math.Abs(from.Rotation - to.Rotation), rotationCount - Math.Abs(from.Rotation - to.Rotation)));
        }
    }
}
