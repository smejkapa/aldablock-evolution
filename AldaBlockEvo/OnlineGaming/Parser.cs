﻿using System;
using System.Linq;
using AldaBlockEvo.OnlineGaming.Commands;
using AldaBlockEvo.GameNS;

namespace AldaBlockEvo.OnlineGaming
{
    class Parser : IDisposable
    {
        public EngineCommand PollCommand(BotCommandReceiver receiver)
        {
            var line = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(line))
                return null;

            EngineCommand command = null;
            var parse = line.Split(' ');
            switch (parse[0])
            {
                case "settings":
                    command = new SettingsCommand(receiver.Bot.MatchSettings, parse[1], parse[2]);
                    break;
                case "update":
                    if (parse[1] == "game")
                    {
                        command = new GameStateCommand(receiver.Bot.GameState, parse[2], parse[3]);
                    }
                    else
                    {
                        if (receiver.Bot.MatchSettings.PlayerNames.Contains(parse[1]))
                        {
                            if (!receiver.Bot.Players.ContainsKey(parse[1]))
                            {
                                receiver.Bot.Players.Add(parse[1], new PlayerState());
                            }

                            var player = receiver.Bot.Players[parse[1]];
                            command = new PlayerCommand(player, parse[1], parse[2], parse[3]);
                        }
                        else
                        {
                            ErrorLog.Log.WriteLine("Invalid player: '{0}'", parse[1]);
                        }
                    }
                    break;
                case "action":
                    command = new BotCommand(receiver, parse[1], parse[2]);
                    break;
            }

            if (command == null)
            {
                ErrorLog.Log.WriteLine("Invalid command: '{0}'", parse[0]);
            }

            return command;
        }

        public void Dispose()
        {
        }
    }
}
