﻿using AldaBlockEvo.GameNS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo.OnlineGaming.Commands
{
    class BotCommandReceiver : EngineCommandReceiver, IDisposable
    {
        private Stopwatch movesTime;

        public Bot Bot {get; private set; }

        public BotCommandReceiver()
        {
            RouteCommand<BotCommand>(ReceiveCommand);

            movesTime = new Stopwatch();

            Bot = new Bot(new RatingWeights(GlobalSettings.CURRENT_WEIGHTS));
        }

        public void Run()
        {
            Console.SetIn(new StreamReader(Console.OpenStandardInput(512)));
            using (var parser = new Parser())
            {
                EngineCommand command;
                while ((command = parser.PollCommand(this)) != null)
                {
                    if (command.Receiver != null)
                        command.Execute();
                }
            }
        }

        public void ReceiveCommand(BotCommand command)
        {
            switch (command.Key)
            {
                // Bot logic should be implemented here
                case "moves":
                    {
#if DEBUG_LOG
						ErrorLog.Log.WriteLine("Preparing moves for round {0}. Time left: {1} ms", GameState.Round, command.Time);
#endif
                        movesTime.Restart();
                        var moves = Bot.MovesForRound(int.Parse(command.Time)).Moves;
                        movesTime.Stop();
#if DEBUG_LOG
						ErrorLog.Log.WriteLine("{0} moves found in: {1} ms", moves.Count(), movesTime.ElapsedMilliseconds);
#endif

                        if (moves.Any())
                        {
                            Console.WriteLine(string.Join(",", moves).ToLower());
                        }
                        else
                        {
                            Console.WriteLine("no_moves");
                        }
                        break;
                    }
                default:
                    {
                        ErrorLog.Log.WriteLine("Invalid bot command: {0}", command.Key);
                        break;
                    }
            }
        }

        public void Dispose()
        {
        }
    }
}
