﻿namespace AldaBlockEvo.OnlineGaming.Commands
{
    class BotCommand : EngineCommand
    {
        public string Key { get; private set; }
        public string Time { get; private set; }

        public BotCommand(EngineCommandReceiver receiver, string key, string time)
            : base(receiver)
        {
            Key = key;
            Time = time;
        }
    }
}
