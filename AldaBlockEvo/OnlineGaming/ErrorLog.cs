﻿using System;
using System.IO;

namespace AldaBlockEvo.OnlineGaming
{
    static class ErrorLog
    {
        public static StreamWriter Log { get; private set; }
        static ErrorLog()
        {
            Log = new StreamWriter(Console.OpenStandardError());
            Log.AutoFlush = true;
        }
    }
}
