﻿using AldaBlockEvo.GameNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AldaBlockEvo
{
    public static class Extensions
    {
        public static void QuickShuffle<T>(this IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = Helpers.TSRandom.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        internal static List<RatingWeights> AsWeights(this List<IBot> bots)
        {
            List<RatingWeights> weights = new List<RatingWeights>();
            for (int i = 0; i < bots.Count; i++)
            {
                weights.Add(bots[i].GetWeights);
            }
            return weights;
        }
    }
}
