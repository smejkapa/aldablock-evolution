﻿using System;
using System.Runtime.Serialization;

namespace AldaBlockEvo
{
    [Serializable]
    public class InvalidPieceStateException : Exception
    {
        public InvalidPieceStateException()
        {
        }
        public InvalidPieceStateException(string message) : base(message)
        {
        }
        public InvalidPieceStateException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected InvalidPieceStateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class InvalidMoveException : Exception
    {
        public InvalidMoveException()
        {
        }
        public InvalidMoveException(string message) : base(message)
        {
        }
        public InvalidMoveException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected InvalidMoveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class InvalidPopulationSizeException : Exception
    {
        public InvalidPopulationSizeException()
        {
        }
        public InvalidPopulationSizeException(string message) : base(message)
        {
        }
        public InvalidPopulationSizeException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected InvalidPopulationSizeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    [Serializable]
    public class InvalidFieldDimensionException : Exception
    {
        public InvalidFieldDimensionException()
        {
        }
        public InvalidFieldDimensionException(string message) : base(message)
        {
        }
        public InvalidFieldDimensionException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected InvalidFieldDimensionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
