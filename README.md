# AldaBlock Evolution #

My entry to theaigames.com AiBlockBattle competition.

## Description ##
There are *three* projects in this repository under one VS2015 solution.

* **AldaBlockEvo** - The main project where all the important code is.

* **AldaBlockEvoTest** - Project with tests for AldaBlockEvo

* BlockEvoModel - Just the evolution algorithm is designed here as a diagram. Not really usefull project (basically just me playing around with VS).

Various useful tools may be found in *Tools* folder.

Ideas and not yet implemented design may be found in *EvolutionDesign.txt*

## Setup ##

All you need to do is open the AldaBlockEvo.sln in VS2015
**Note:** There are C# 6.0 features in the code, so earlier VS versions may not be able to compile it correctly.

## Usage ##
There are several compile configurations.
Most of them influence only the Program.cs.
Only PlayOnline resp. Verbose alter some compilation resp. logging.

### Tools ###
** Note ** To use Python scripts [Python 3.x.x](https://www.python.org/downloads/) needs to be installed. 

To use bash scripts - .sh files need to be associated with bash. You can use git bash or cygwin on Windows for example.

* **CreateTarToUpload.sh** bash script that creates a tar.gz archive to upload to the competition. (There are only source files in the archive and the #define PLAY_ONLINE is uncommented wherever needed)

* **WeightSorter.py** Looks into C:\TetrisEvo\ for output files from last (or specified) evolution run and sorts the weights in descending order - making the selection of the best of the best easier.
### Solution configurations ###

**Evolution** - Runs the evolution of the weights of the rating function.
Evolution parameters may be setup in the constructor of the Evolution class.
Logs all kinds of useful (debug) information into LOG_DIR that one can set in the GlobalSettings class. (C:\TetrisEvo\ by default)

* **PlayOnline** is mainly used to play in the aigames.com competition.
You can also use this to communicate with the bot in a way the site does - through command line.
**Note:** For "production version" (the one that is uploaded to the competition) - PLAY_ONLINE macro must be defined in all files where it is commented out.
This is because there is no C# 6.0 support on thegaigames.com and there is also no way to use solution configurations there. - **All is done automatically if CreateTarToUpload.sh is used.**

* **Multiplayer** - Takes current weights defined in GlobalSettings.CURRENT_WEIGHTS and weights defined in GlobalSettings.WEIGHTS_TXT to create two bots.
Then these two bots participate in a duel.

* **Single Player (SP)** - Takes current weights defined in GlobalSettings.CURRENT_WEIGHTS to create a bot that plays a single player tetris games.
This involves solid line addition (as done in the competition) but no the garbage lines are added as often as defined in GlobalSettings.GARBAGE_LINE_ADD_RATE.

## Inspiration ##
* [An evolutionary approach to tetris](https://www2.cs.fau.de/EN/publication/download/mic.pdf)
* [Genetic Algorithm Performance with Different Selection Strategies in Solving TSP](http://umpir.ump.edu.my/2609/1/WCE_noraini.pdf)