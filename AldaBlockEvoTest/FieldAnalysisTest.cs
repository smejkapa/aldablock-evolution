﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AldaBlockEvo.GameNS;
using static AldaBlockEvo.GlobalSettings;

namespace AldaBlockEvoTest
{
    [TestClass]
    public class FieldAnalysisTest
    {
        [TestMethod]
        public void SimpleFieldAnalysis()
        {
            // Arrange
            Field field = new Field();
            FieldScore score = new FieldScore(0, 0, 0, false, 0);
            FieldAnalysis expected = new FieldAnalysis();

            // Act
            var actual = FieldAnalyzer.AnalyzeField(field, score);

            // Assert
            Assert.AreEqual(expected, actual, "Field not analyzed correctly.");
        }
    }
}
