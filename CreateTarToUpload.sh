#!/bin/bash

DIRECTORY="./tmp_upload"

if [ -d "$DIRECTORY" ]; then
  echo "Directory $DIRECTORY already exists."
  read -p "Delete directory $DIRECTORY? (y/n) " -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "Terminating process, dir not deleted"
    exit 0
  fi
fi

rm -f -r $DIRECTORY
mkdir $DIRECTORY
cp ./AldaBlockEvo.sln $DIRECTORY
cp -r ./AldaBlockEvo $DIRECTORY
rm -f -r $DIRECTORY/AldaBlockEvo/bin
rm -f -r $DIRECTORY/AldaBlockEvo/obj
rm -f upload.tar.gz
find $DIRECTORY -type f -name '*.cs' -exec sed -i 's/\/\/#define PLAY_ONLINE/#define PLAY_ONLINE/' {} \;
tar -czf upload.tar.gz $DIRECTORY
rm -f -r $DIRECTORY
echo "Archive created!"
